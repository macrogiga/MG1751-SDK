;******************** (C) COPYRIGHT 2019 Macrogiga Electronics******************
;* File Name          : startup_mg.s
;* Author             : 
;* Version            : V1.0.0
;* Date               : 2019
; 
;*******************************************************************************
;

Stack_Size      EQU     0x00000800

                AREA    STACK, NOINIT, READWRITE, ALIGN=3
Stack_Mem       SPACE   Stack_Size
__initial_sp


; <h> Heap Configuration
;   <o>  Heap Size (in Bytes) <0x0-0xxxX00:8>
; </h>

Heap_Size       EQU     0x00000400

                AREA    HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem        SPACE   Heap_Size
__heap_limit

                PRESERVE8
                THUMB


; Vector Table Mapped to Address 0 at Reset
                AREA    RESET, DATA, READONLY
                EXPORT  __Vectors
                EXPORT  __Vectors_End
                EXPORT  __Vectors_Size
                IMPORT  RemapTab

__Vectors       DCD     __initial_sp                   ; Top of Stack
                DCD     Reset_Handler                  ; Reset Handler
                DCD     RemapTab+1                     ; NMI Handler            jump to RemapTab[0][0]
                DCD     RemapTab+5                     ; Hard Fault Handler     jump to RemapTab[1][0]
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     RemapTab+9                     ; SVCall Handler         jump to RemapTab[2][0]
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     RemapTab+13                    ; PendSV Handler
                DCD     RemapTab+17                    ; SysTick Handler

                ; External Interrupts
                DCD     RemapTab+21                    ; Baseband Seg irq handler / led pwm feed data irq
                DCD     RemapTab+25                    ; UART irq handler
                DCD     RemapTab+29                    ; BB-wakeup&sleep irq handler
                DCD     RemapTab+33                    ; RTC irq handler
                DCD     RemapTab+37                    ; watch dog irq handler
                DCD     RemapTab+41                    ; low voltage power irq handler 
                DCD     RemapTab+45                    ; GPIO irq handler
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     RemapTab+49                    ; timer irq handler
                DCD     0                              ; Reserved
                DCD     RemapTab+53                    ; I2C irq handler
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                
__Vectors_End

__Vectors_Size  EQU  __Vectors_End - __Vectors

                AREA    |.text|, CODE, READONLY

; Reset handler routine
Reset_Handler   PROC
                EXPORT  Reset_Handler                 [WEAK]
                IMPORT  __main


                LDR     R0, =__initial_sp          ; set stack pointer 
                MSR     MSP, R0  

                LDR     R0, =__main
                BX      R0
                ENDP


                ALIGN

;*******************************************************************************
; User Stack and Heap initialization
;*******************************************************************************
                 IF      :DEF:__MICROLIB
                
                 EXPORT  __initial_sp
                 EXPORT  __heap_base
                 EXPORT  __heap_limit
                
                 ELSE
                
                 IMPORT  __use_two_region_memory
                 EXPORT  __user_initial_stackheap
                 
__user_initial_stackheap

                 LDR     R0, =  Heap_Mem
                 LDR     R1, =(Stack_Mem + Stack_Size)
                 LDR     R2, = (Heap_Mem +  Heap_Size)
                 LDR     R3, = Stack_Mem
                 BX      LR

                 ALIGN

                 ENDIF

                 END

;************************ (C) COPYRIGHT Macrogiga Electornics *****END OF FILE*****
