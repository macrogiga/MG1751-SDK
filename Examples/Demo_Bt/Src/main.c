/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include <stdio.h>
#include "mg_driver.h"


extern void test_ble_stack_slave(void);
extern void test_ble_stack_master(void);
//extern void test_ble_carrier_tone(void);

u32 mydelay(u32 num)
{
    u32 i,j=0;
    
    for(i = 0 ; i < num ; i ++)
    {
        j += i;
    }
    
    return j;
}

int main(void)
{
    mydelay(0xa00000*3);
//    MgBle_Dbg_Spi_init(); //enable dbg spi port,A3(cs),A4(clk),A9(MI),A15(MO)
    
    NVIC_EnableIRQ(INT_MASK_BIT_WATCHDOG);    
    WDog_Init(5*32768);  //irq enabled, clock div = 1; clk:32768Hz  
//    PA_IO_init();

    test_ble_stack_slave();
//    test_ble_stack_master();

//    test_ble_carrier_tone();
    
    while(1);  //never be here
}
