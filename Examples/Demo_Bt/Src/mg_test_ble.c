/**
  ******************************************************************************
  * @file    mg_test_ble.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"
#include "mg_ble_control.h"



void PA_IO_init(void)
{
    GPIO_InitTypeDef init;
    u32 delay = 0x200000;
    
    while(delay--); //for SWD ports' safe usage ONLY!!!
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A7,GPIO_MAP_PIN_FUNC3);
    GPIO_FunctionSelect(GPIO_MAP_PIN_A8,GPIO_MAP_PIN_FUNC3);
    
    init.GPIO_Pin = GPIO_PIN_7 | GPIO_PIN_8;
    init.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIO, &init);
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A7,GPIO_MAP_PIN_FUNC2);/*TX*/
    GPIO_FunctionSelect(GPIO_MAP_PIN_A8,GPIO_MAP_PIN_FUNC2);/*RX*/
}

void toggle_led_io(void)
{
    static u8 high = 0;
    
    if(high)GPIO->DATA_OUT_SET = GPIO_PIN_10;
    else GPIO->DATA_OUT_CLEAR = GPIO_PIN_10;
    
    high = !high;
}

void led_io_init(void)
{
    GPIO_InitTypeDef cfg;
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A10,GPIO_MAP_PIN_FUNC3);
    
    cfg.GPIO_Mode = GPIO_Mode_Out_PP;
    cfg.GPIO_Pin  = GPIO_PIN_10;
    GPIO_Init(GPIO, &cfg);
    
    GPIO->DATA_OUT_CLEAR = GPIO_PIN_10;
}

void key_io_init(void)
{
    GPIO_InitTypeDef cfg;
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A0,GPIO_MAP_PIN_FUNC1);
    
    cfg.GPIO_Mode = GPIO_Mode_IPD;
    cfg.GPIO_Pin  = GPIO_PIN_0;
    GPIO_Init(GPIO, &cfg);
}

////// !!! porting function, this will be invoked by ble lib. !!!/////
void slave_app_cfg(void)
{
    u8 adv[] = {2,1,6, 7,9,'M','G','1','7','5','1'};
    u8 rsp[] = {5,0xff,'m','g','1','7'};
    
    BLE_ADV_DATA_INFO slave_cfg;
    
    slave_cfg.advType = ADV_IND;//ADV_NONCONN_IND;//ADV_IND;//ADV_SCAN_IND//ADV_DIRECT_IND
    slave_cfg.advData = adv;
    slave_cfg.advDataLen = sizeof(adv);    
    slave_cfg.scanResponseData= rsp;    
    slave_cfg.scanResponseDataLen = sizeof(rsp);
    slave_cfg.interval = 160; //100ms
    slave_cfg.txAddrType = ADDR_TYPE_RANDOM;
    slave_cfg.directIndTargetAddr[0] = 0xaa; //target addr if any
    slave_cfg.directIndTargetAddr[1] = 0xbb;
    slave_cfg.directIndTargetAddrType = 1;

    MgBle_Periph_Init(&slave_cfg);
}

void test_ble_stack_slave(void)
{
    u8 *ble_addr;
    
    led_io_init(); //debug purpose ONLY
    UART_Set_baudrate(115200);
    
    //================== BLE Base band init ================
    MgBle_Init(TXPWR_0DBM, &ble_addr);
    
    
    //============ clear MCU pending irq if any ============
    NVIC_IrqClearPendingIRQ(BBSeg_IRQn);
    NVIC_IrqClearPendingIRQ(BB_IRQn);
    
    //============== Enable NVIC IRQ bits ==================
    NVIC_EnableIRQ(INT_MASK_BIT_BB_SEG);
    NVIC_EnableIRQ(INT_MASK_BIT_BB);
    
    //================== BLE Slave role init ===============
    slave_app_cfg(); //cfg the slave
    
    //================ BLE Slave stack running =============
    while(1)
    {
        //*** user task inserted here ***
        //to do...
        
        SysGotoStopMode(); //MCU goto power saving mode
    }
    
    //never be here
}

