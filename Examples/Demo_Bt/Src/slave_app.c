/**
  ******************************************************************************
  * @file    slave_app.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include <string.h>
#include "mg_driver.h"
#include "mg_ble_control.h"




///////////////////////////////////////////////////////////////////////////////////////////////
void ser_write_rsp_pkt(u8 pdu_type); /*if the related character has the property of WRITE(with response) or TYPE_CFG, one MUST invoke this func*/
void att_notFd(u8 pdu_type, u8 attOpcode, u16 attHd );
void att_server_rdByGrTypeRspDeviceInfo(u8 pdu_type);
void att_server_rdByGrTypeRspPrimaryService(u8 pdu_type, u16 start_hd, u16 end_hd, u8*uuid, u8 uuidlen);
void att_server_rd( u8 pdu_type, u8 attOpcode, u16 att_hd, u8* attValue, u8 datalen );


#define UUID16_FORMAT  0xff


#define SOFTWARE_INFO "SV2.0.1"
//#define MANU_INFO     "MacroGiga Bluetooth"
#define DeviceInfo    "MG1751"  /*max len is 24 bytes*/

extern u8 StartEncryption; //defined in lib
u16 cur_notifyhandle = 0x12;  //Note: make sure each notify handle by invoking function: set_notifyhandle(hd);

u8* getDeviceInfoData(u8* len)
{    
    *len = sizeof(DeviceInfo);
    return (u8*)DeviceInfo;
}

//u8* getsoftwareversion(void)
//{
//    return (u8*)SOFTWARE_INFO;
//}

/**********************************************************************************
                 *****DataBase****

01 - 06  GAP (Primary service) 0x1800
  03:04  name
07 - 0f  Device Info (Primary service) 0x180a
  0a:0b  firmware version
  0e:0f  software version
10 - 19  LED service (Primary service) 00010203-0405-0607-0809-0a0b0c0d1910
  11:12  00010203-0405-0607-0809-0a0b0c0d1911(0x1A)  Status
  13     cfg
  14:15  00010203-0405-0607-0809-0a0b0c0d1912(0x0A)  CMD
  16     cfg
  17:18  00010203-0405-0607-0809-0a0b0c0d1913(0x0A)  OTA
  19     0x2901  info
************************************************************************************/

typedef struct ble_character16{
    u16 type16;          //type2
    u16 handle_rec;      //handle
    u8  characterInfo[5];//property1 - handle2 - uuid2
    u8  uuid128_idx;     //0xff means uuid16,other is idx of uuid128
}BLE_CHAR;

typedef struct ble_UUID128{
    u8  uuid128[16];//uuid128 string: little endian
}BLE_UUID128;

//
///STEP0:Character declare
//
const BLE_CHAR AttCharList[] = {
// ======  gatt =====  Do NOT Change!!
    {TYPE_CHAR,0x03,ATT_CHAR_PROP_RD, 0x04,0,0x00,0x2a,UUID16_FORMAT},//name
    //05-06 reserved
// ======  device info =====    Do NOT Change if using the default!!!  
    {TYPE_CHAR,0x08,ATT_CHAR_PROP_RD, 0x09,0,0x29,0x2a,UUID16_FORMAT},//manufacture
    {TYPE_CHAR,0x0a,ATT_CHAR_PROP_RD, 0x0b,0,0x26,0x2a,UUID16_FORMAT},//firmware version
    {TYPE_CHAR,0x0e,ATT_CHAR_PROP_RD, 0x0f,0,0x28,0x2a,UUID16_FORMAT},//sw version
    
// ======  LED service or other services added here =====  User defined  
    {TYPE_CHAR,0x11,ATT_CHAR_PROP_W|ATT_CHAR_PROP_RD|ATT_CHAR_PROP_NTF, 0x12,0, 0,0, 1/*uuid128-idx1*/ },//status
    {TYPE_CFG, 0x13,ATT_CHAR_PROP_RD|ATT_CHAR_PROP_W},//cfg    
    {TYPE_CHAR,0x14,ATT_CHAR_PROP_W|ATT_CHAR_PROP_RD,                   0x15,0, 0,0, 2/*uuid128-idx2*/ },//cmd    
    {TYPE_CHAR,0x17,ATT_CHAR_PROP_W|ATT_CHAR_PROP_RD,                   0x18,0, 0,0, 3/*uuid128-idx3*/ },//others
    {TYPE_INFO, 0x19,ATT_CHAR_PROP_RD},//description,
};

const BLE_UUID128 AttUuid128List[] = {
    {0x10,0x19,0x0d,0xc,0xb,0xa,9,8,7,6,5,4,3,2,1,0}, //idx0,little endian, service uuid
    {0x11,0x19,0x0d,0xc,0xb,0xa,9,8,7,6,5,4,3,2,1,0}, //idx1,little endian, character status uuid
    {0x12,0x19,0x0d,0xc,0xb,0xa,9,8,7,6,5,4,3,2,1,0}, //idx2,little endian, character cmd uuid
    {0x13,0x19,0x0d,0xc,0xb,0xa,9,8,7,6,5,4,3,2,1,0}, //idx3,little endian, character OTA uuid
};

u8 GetCharListDim(void)
{
    return sizeof(AttCharList)/sizeof(AttCharList[0]);
}

//////////////////////////////////////////////////////////////////////////
///STEP1:Service declare
// read by type request handle, primary service declare implementation
void att_server_rdByGrType( u8 pdu_type, u8 attOpcode, u16 st_hd, u16 end_hd, u16 att_type )
{
 //!!!!!!!!  hard code for gap and gatt, make sure here is 100% matched with database:[AttCharList] !!!!!!!!!

    if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd == 1))//hard code for device info service
    {
        att_server_rdByGrTypeRspDeviceInfo(pdu_type);//using the default device info service
        //apply user defined (device info)service example
        //{
        //    u8 t[] = {0xa,0x18};
        //    att_server_rdByGrTypeRspPrimaryService(pdu_type,0x7,0xf,(u8*)(t),2);
        //}
        return;
    }
    
    //hard code
    else if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd <= 0x10)) //usr's service
    {
        att_server_rdByGrTypeRspPrimaryService(pdu_type,0x10,0x19,(u8*)(AttUuid128List[0].uuid128),16);
        return;
    }
    //other service added here if any
    //to do....

    ///error handle
    att_notFd( pdu_type, attOpcode, st_hd );
}

///STEP2:data coming
///write response, data coming....
void ser_write_rsp(u8 pdu_type/*reserved*/, u8 attOpcode/*reserved*/, 
                   u16 att_hd, u8* attValue/*app data pointer*/, u8 valueLen_w/*app data size*/)
{
    switch(att_hd)
    {
        default:
            ser_write_rsp_pkt(pdu_type);
//            att_notFd( pdu_type, attOpcode, att_hd );	/*the default response, also for the purpose of error robust */
            break;
    }
 }

 ///STEP2.1:Queued Writes data if any
void ser_prepare_write(u16 handle, u8* attValue, u16 attValueLen, u16 att_offset)//user's call back api 
{
    //queued data:offset + data(size)
    //when ser_execute_write() is invoked, means end of queue write.
    
    //to do
}
 
void ser_execute_write(void)//user's call back api 
{
    //end of queued writes  
    
    //to do...    
}

///STEP3:Read data
//// read response
void server_rd_rsp(u8 attOpcode, u16 attHandle, u8 pdu_type)
{
    switch(attHandle) //hard code
    {
        case 0x04: //name
            att_server_rd( pdu_type, attOpcode, attHandle, (u8*)(DeviceInfo), sizeof(DeviceInfo)-1);
            break;
        
        case 0x09: //MANU_INFO
            //att_server_rd( pdu_type, attOpcode, attHandle, (u8*)(MANU_INFO), sizeof(MANU_INFO)-1);
            att_server_rd( pdu_type, attOpcode, attHandle, MgBle_GetBleStackVersion(), strlen((const char*)MgBle_GetBleStackVersion())); //ble lib build version
        
            break;
        
        case 0x0b: //FIRMWARE_INFO
        {            
            //do NOT modify this code!!!
            att_server_rd( pdu_type, attOpcode, attHandle, GetFirmwareInfo(),strlen((const char*)GetFirmwareInfo()));
            break;
        }
        
        case 0x0f://SOFTWARE_INFO
            att_server_rd( pdu_type, attOpcode, attHandle, (u8*)(SOFTWARE_INFO), sizeof(SOFTWARE_INFO)-1);
            break;
        
        case 0x13://cfg
            {
                u8 t[2] = {0,0};
                att_server_rd( pdu_type, attOpcode, attHandle, t, 2);
            }
            break;
            
        case 0x19://description
            {
                att_server_rd( pdu_type, attOpcode, attHandle, (u8*)"mg description", 14);
            }
            break;
        
        default:
            att_notFd( pdu_type, attOpcode, attHandle );/*the default response, also for the purpose of error robust */
            break;
    }
}

//本回调函数可用于蓝牙模块端主动发送数据之用，协议栈会在系统允许的时候（异步）回调本函数，不得阻塞！！
void Gatt_UserSendNotify(void)
{
    //to do if any ...
    //add user sending data notify operation ....
}

void UsrProcCallback(u8 evt, u8* contxt) //invoked at each BB sleep tick
{
    //TXRX_IDLE, NULL
}

void MgBle_ConnStatusUpdate(u8 status /*PERIPH_CONNECT_XXXXX*/) //porting api
{
    switch(status)
    {
        case PERIPH_CONNECT_CONNECTED:
            break;
        
        case PERIPH_CONNECT_DISCONNECTED:
            break;
        
        default:break; //error
    }
}

void server_blob_rd_rsp(u8 attOpcode, u16 attHandle, u8 dataHdrP, u16 offset)
{
//    u16 size;
//    
//    if(attHandle == 0x22)//hid report map
//    {
//        if(offset + 0x16 <= HID_MAP_SIZE)size = 0x16;
//        else size = HID_MAP_SIZE - offset;
//        att_server_rd( dataHdrP, attOpcode, attHandle, (u8*)(Hid_map+offset), size);
//        
//        return;
//    }
//    
//    att_notFd( dataHdrP, attOpcode, attHandle);/*the default response, also for the purpose of error robust */
}


//return 1 means found
su32 GetPrimaryServiceHandle(u16 hd_start, u16 hd_end,
                            u16 uuid16,   
                            u16* hd_start_r,u16* hd_end_r)
{
// example
    if((uuid16 == 0x1812) && (hd_start <= 0x19))// MUST keep match with the information saved in function  att_server_rdByGrType(...)
    {
        *hd_start_r = 0x19;
        *hd_end_r = 0x2a;
        return 1;
    }
    
    return 0;
}

