/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
  
#include "mg_driver_sys.h"

/*****************************************************************************************
                   
                   !!!! IMPORTANT !!!!
                   
1)Do NOT change any code if you do NOT know what you are doing!
2)Default interface is HCI cmd format at 9600bps.
3)Global macro definition [UART_2_WIRE_FORMAT] controls BLE 2-wire cmd format.
4)GPA5-TX   GPA6-RX, flow control is optional.
*****************************************************************************************/

extern void test_ble_direct_test(void);

int main(void)
{
    test_ble_direct_test();

    while(1);  //never be here    
}
