/**
  ******************************************************************************
  * @file    mg_ble_int.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */

#include "mg_driver.h"
#include "mg_ble_control.h"

u8 MgBle_DirctTestRx_Run(void);
u8 MgBle_DirctTestTx_Run(u8);

void radio_seg_rx_tx_irq(void);
void BBSeg_IRQHandler(void)
{
    radio_seg_rx_tx_irq();
}

extern u16 RX_COUNT_LE_TEST;

void BB_IRQHandler(void)
{    
//    MgBle_DirctTestTx_Run(0); 
//    RX_COUNT_LE_TEST += MgBle_DirctTestRx_Run();
}
