/**
  ******************************************************************************
  * @file    hogp_app.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include <string.h>
#include "mg_driver.h"
#include "mg_ble_control.h"


u8 sconn_notifydata(u8* data, u8 len);//returns data size has been sent. ******* user's safe API *********

///////////////////////////////////////////////////////////////////////////////////////////////
void ser_write_rsp_pkt(u8 pdu_type); /*if the related character has the property of WRITE(with response) or TYPE_CFG, one MUST invoke this func*/
void att_notFd(u8 pdu_type, u8 attOpcode, u16 attHd );
void att_server_rdByGrTypeRspDeviceInfo(u8 pdu_type);
void att_server_rdByGrTypeRspPrimaryService(u8 pdu_type, u16 start_hd, u16 end_hd, u8*uuid, u8 uuidlen);
void att_server_rd( u8 pdu_type, u8 attOpcode, u16 att_hd, u8* attValue, u8 datalen );
u8* get_ble_version(void);

/// Characteristic Properties Bit
#define ATT_CHAR_PROP_RD                            0x02
#define ATT_CHAR_PROP_W_NORSP                       0x04
#define ATT_CHAR_PROP_W                             0x08
#define ATT_CHAR_PROP_NTF                           0x10
#define ATT_CHAR_PROP_IND                           0x20 
#define GATT_PRIMARY_SERVICE_UUID                   0x2800

#define TYPE_CHAR      0x2803
#define TYPE_CFG       0x2902
#define TYPE_INFO      0x2901
#define TYPE_xRpRef    0x2907
#define TYPE_RpRef     0x2908
#define TYPE_INC       0x2802
#define UUID16_FORMAT  0xff


#define SOFTWARE_INFO "SV2.0.1"
#define MANU_INFO     "MacroGiga Bluetooth"
#define DeviceInfo    "mg1751-hid"  /*max len is 24 bytes*/

extern u8 StartEncryption; //defined in lib
u16 cur_notifyhandle = 0x1e;  //Note: make sure each notify handle by invoking function: set_notifyhandle(hd);

u8* getDeviceInfoData(u8* len)
{    
    *len = sizeof(DeviceInfo);
    return (u8*)DeviceInfo;
}

u8* getsoftwareversion(void)
{
    return (u8*)SOFTWARE_INFO;
}

/**********************************************************************************
                 *****DataBase****

01 - 06  GAP (Primary service) 0x1800
  03:04  name
07 - 0f  Device Info (Primary service) 0x180a
  0a:0b  firmware version
  0e:0f  software version
************************************************************************************/

typedef struct ble_character16{
    u16 type16;          //type2
    u16 handle_rec;      //handle
    u8  characterInfo[5];//property1 - handle2 - uuid2
    u8  uuid128_idx;     //0xff means uuid16,other is idx of uuid128
}BLE_CHAR;

typedef struct ble_UUID128{    
    u8  uuid128[16];//uuid128 string: little endian
}BLE_UUID128;

//
///STEP0:Character declare
//
const BLE_CHAR AttCharList[] = {
// ======  gatt =====  Do NOT Change!!
    {TYPE_CHAR,0x03,ATT_CHAR_PROP_RD, 0x04,0,0x00,0x2a,UUID16_FORMAT},//name
    //05-06 reserved
// ======  device info =====    Do NOT Change if using the default!!!  
    {TYPE_CHAR,0x08,ATT_CHAR_PROP_RD, 0x09,0,0x29,0x2a,UUID16_FORMAT},//manufacture
    {TYPE_CHAR,0x0a,ATT_CHAR_PROP_RD, 0x0b,0,0x26,0x2a,UUID16_FORMAT},//firmware version
    {TYPE_CHAR,0x0c,ATT_CHAR_PROP_RD, 0x0d,0,0x50,0x2a,UUID16_FORMAT},//PnPID
    {TYPE_CHAR,0x0e,ATT_CHAR_PROP_RD, 0x0f,0,0x28,0x2a,UUID16_FORMAT},//sw version
    
    // ======  HID =====
    //    {TYPE_INC, 0x1a,},//include, hard code!!!
    {TYPE_INC, 0x001a,0x2b,0x00, 0x2d,0x00, 0x0a,0xa0}, //hard code uuid-0aa0, I just share the memory
    {TYPE_CHAR,0x1B,ATT_CHAR_PROP_RD|ATT_CHAR_PROP_W_NORSP,0x1c,0, 0x4e,0x2a,UUID16_FORMAT},//protocol mode
    {TYPE_CHAR,0x1d,ATT_CHAR_PROP_W|ATT_CHAR_PROP_RD|ATT_CHAR_PROP_NTF, 0x1e,0,0x4d,0x2a,UUID16_FORMAT},//report
    {TYPE_CFG, 0x1f,ATT_CHAR_PROP_RD|ATT_CHAR_PROP_W},//cfg
    {TYPE_RpRef,0x20,ATT_CHAR_PROP_RD},//Report Reference
    
    {TYPE_CHAR,0x21,ATT_CHAR_PROP_RD, 0x22,0,0x4b,0x2a,UUID16_FORMAT},//report map
    {TYPE_xRpRef,0x23,ATT_CHAR_PROP_RD},//External Report Reference
    
    {TYPE_CHAR,0x24,ATT_CHAR_PROP_W|ATT_CHAR_PROP_RD|ATT_CHAR_PROP_NTF,0x25,0,0x33,0x2a,UUID16_FORMAT},//boot mouse input report
    {TYPE_CFG, 0x26,ATT_CHAR_PROP_RD|ATT_CHAR_PROP_W},//cfg
    
    {TYPE_CHAR,0x27,ATT_CHAR_PROP_RD,                      0x28,0,0x4a,0x2a,UUID16_FORMAT},//hid info
    {TYPE_CHAR,0x29,ATT_CHAR_PROP_W_NORSP,                 0x2a,0,0x4c,0x2a,UUID16_FORMAT},//hid control point

// ======  0xa00a  ======    include uuid
    {TYPE_CHAR,0x2c,ATT_CHAR_PROP_RD, 0x2d,0,0x0b,0x0a,UUID16_FORMAT},//
};

const BLE_UUID128 AttUuid128List[] = {
    {0x10,0x19,0x0d,0xc,0xb,0xa,9,8,7,6,5,4,3,2,1,0}, //idx0,little endian, service uuid, not used.
};

u8 GetCharListDim(void)
{
    return sizeof(AttCharList)/sizeof(AttCharList[0]);
}

//////////////////////////////////////////////////////////////////////////
///STEP1:Service declare
// read by type request handle, primary service declare implementation
void att_server_rdByGrType( u8 pdu_type, u8 attOpcode, u16 st_hd, u16 end_hd, u16 att_type )
{
 //!!!!!!!!  hard code for gap and gatt, make sure here is 100% matched with database:[AttCharList] !!!!!!!!!

    if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd == 1))//hard code for device info service
    {
        att_server_rdByGrTypeRspDeviceInfo(pdu_type);//using the default device info service
        //apply user defined (device info)service example
        //{
        //    u8 t[] = {0xa,0x18};
        //    att_server_rdByGrTypeRspPrimaryService(pdu_type,0x7,0xf,(u8*)(t),2);
        //}
        return;
    }
    else if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd <= 0x19)) //usr's service
    {
        u8 hid[2] = {0x12,0x18};
        att_server_rdByGrTypeRspPrimaryService(pdu_type,0x19,0x2a,(u8*)(hid),2);
        return;
    }
    else if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd <= 0x2b)) //usr's service
    {
        u8 inc[2] = {0x0a,0xa0};
        att_server_rdByGrTypeRspPrimaryService(pdu_type,0x2b,0x2d,(u8*)(inc),2);
        return;
    }
    
    //other service added here if any
    //to do....

    ///error handle
    att_notFd( pdu_type, attOpcode, st_hd );
}

u8 CanNotifyFlag = 0;

///STEP2:data coming
///write response, data coming....
void ser_write_rsp(u8 pdu_type/*reserved*/, u8 attOpcode/*reserved*/, 
                   u16 att_hd, u8* attValue/*app data pointer*/, u8 valueLen_w/*app data size*/)
{
    switch(att_hd)
    {
        case 0x1f://cfg
            CanNotifyFlag = 1;
        case 0x26:
            ser_write_rsp_pkt(pdu_type);
            break;
        
        default:
            ser_write_rsp_pkt(pdu_type);
//            att_notFd( pdu_type, attOpcode, att_hd );	/*the default response, also for the purpose of error robust */
            break;
    }
 }

 ///STEP2.1:Queued Writes data if any
void ser_prepare_write(u16 handle, u8* attValue, u16 attValueLen, u16 att_offset)//user's call back api 
{
    //queued data:offset + data(size)
    //when ser_execute_write() is invoked, means end of queue write.
    
    //to do    
}
 
void ser_execute_write(void)//user's call back api 
{
    //end of queued writes  
    
    //to do...    
}

/*********************************** keyboard ***********************************
    0x05, 0x01,//standard keyboard    Usage Page(global)Generic Desktop Controls 
    0x09, 0x06,//                     Usage (local) Keyboard
    0xA1, 0x01, //collection((Application)
    
    0x85, 0x01,//report ID
    0x05, 0x07, //Keyboard/Keypad 
    0x19, 0xE0, //Usage Minimum
    0x29, 0xE7, //Usage Maximum
    0x15, 0,    //Logical Minimum
    0x25, 1,    //Logical Maximum
    
    0x75, 1,    //Report Size(1 bit)
    0x95, 8,    //Report Count(8)
    0x81, 2,    //input(2) - variable absolute data
    
    0x95, 1,    //Report Count(1)
    0x75, 0x08, //Report Size(8 bit)
    0x81, 3,    //input(3) - variable absolute constant, for OEM data usage
    
    0x95, 5,    //Report Count(5)
    0x75, 1,    //Report Size(1 bit)
    5, 8,       //Usage Page(global)LED
    0x19, 1,    //Usage Minimum
    0x29, 5,    //Usage Maximum	
    0x91, 2,    //output(2) - variable absolute data
    
    0x95, 1,    //Report Count(1)
    0x75, 3,    //Report Size(3 bit)
    0x91, 3,    //output(3) - variable absolute constant
    
    0x95, 6,    //Report Count(6)
    0x75, 8,    //Report Size(8 bit)
    0x15, 0,    //Logical Minimum
    0x25, 0xff, //Logical Maximum
    5, 7,       //Keyboard/Keypad 
    0x19, 0,    //Usage Minimum
    0x29, 0xff, //Usage Maximum
    0x81, 0,    //input(0) - array absolute data
    
    0xc0,             //END collection
    
    //format: 0x01, 0x00,0x00, value,0x00,0x00,0x00,0x00,0x00
********************************************************************************/

#define HID_MAP_SIZE (sizeof(Hid_map))
const u8 Hid_map[]= {
    //first Id    
    0x05, 0x0c, //Consumer 
    0x09, 0x01, //Consumer Control  
    
    0xA1, 0x01, //collection
    
    0x85, 0x02,//report ID
    
    //first item start    
    0x09, 0xe9, //V+
    0x09, 0xea, //V- 
    0x09, 0xb5, //next     
    0x09, 0xb6, //pre
    0x09, 0x41, //menu pick
    0x09, 0x46, //menu esc
    
    0x15, 0x00, // Logical Min (0)
    0x25, 0x01, // Logical Max (1)
    0x75, 0x01, // Report Size (1)
    0x95, 0x06, // Report Count (6)
    
    0x81, 0x02,  //input(2) - variable absolute data
    0x75, 0x01,  //Report Size(1 bit)
    0x95, 0x02,  //Report Count(2)
    0x81, 0x02,  //input(2) - variable absolute constant
    
    0xc0,                  //end collection    
}; 

///STEP3:Read data
//// read response
void server_rd_rsp(u8 attOpcode, u16 attHandle, u8 pdu_type)
{
    switch(attHandle) //hard code
    {
        case 0x04: //name
            att_server_rd( pdu_type, attOpcode, attHandle, (u8*)(DeviceInfo), sizeof(DeviceInfo)-1);
            break;
        
        case 0x09: //MANU_INFO
            //att_server_rd( pdu_type, attOpcode, attHandle, (u8*)(MANU_INFO), sizeof(MANU_INFO)-1);
            att_server_rd( pdu_type, attOpcode, attHandle, MgBle_GetBleStackVersion(), strlen((const char*)MgBle_GetBleStackVersion())); //ble lib build version
        
            break;
        
        case 0x0b: //FIRMWARE_INFO
        {            
            //do NOT modify this code!!!
            att_server_rd( pdu_type, attOpcode, attHandle, GetFirmwareInfo(),strlen((const char*)GetFirmwareInfo()));
            break;
        }
        
        case 0x0f://SOFTWARE_INFO
            att_server_rd( pdu_type, attOpcode, attHandle, (u8*)(SOFTWARE_INFO), sizeof(SOFTWARE_INFO)-1);
            break;
        
        case 0x20://Report Reference
            {
            u8 t[2] = {0,1};
            att_server_rd( pdu_type, attOpcode, attHandle, t, 2);
            }
            break;
        
        case 0x22://report map
            if(!StartEncryption)
            {
                att_ErrorFd_eCode(pdu_type, attOpcode, attHandle,0x0F); //pair needed
            }
            else
            {
                att_server_rd( pdu_type, attOpcode, attHandle, (u8*)Hid_map, 0x16);
            }
            break;
        
        case 0x23://external report ref
            {
            u8 t[2] = {0x0b,0x0a};
            att_server_rd( pdu_type, attOpcode, attHandle, t, 2);
            }
            break;

        case 0x28://hid info
         {
            u8 t[4] = {0xdb,0xfa,0x5a,0x02};
            att_server_rd( pdu_type, attOpcode, attHandle, t, 4);
         }
            break;
         
        case 0x1f: //cfg
        case 0x26: //cfg
            {
                u8 t[2] = {0,0};
                att_server_rd( pdu_type, attOpcode, attHandle, t, 2);
            }
            break;
        
        default:
            att_notFd( pdu_type, attOpcode, attHandle );/*the default response, also for the purpose of error robust */
            break;
    }
}

/*report:
        VOL_UP  --> 0x01
        VOL_DOWN    -->  0x02
        NEXT_TRACK  -->  0x04
        PREV_TRACK  -->  0x08
        MENU_OK      -->  0x10
        MENU_EXIT    -->  0x20
*/
static u8 NotifyKey(u8 KeyIdx)//hid standard keyboard key, hardcode
{
    u8 Keyarray[2] = {2, 0};
    
    Keyarray[1] = KeyIdx;
    sconn_notifydata(Keyarray,2);
    
    Keyarray[1] = 0;
    sconn_notifydata(Keyarray,2);
    
    return 1;
}

//本回调函数可用于蓝牙模块端主动发送数据之用，协议栈会在系统允许的时候（异步）回调本函数，不得阻塞！！
void Gatt_UserSendNotify(void)
{
#if 0  //auto shutter
    static u8 count = 0;
    
    count ++;
    
    if(count > 50)
    {
        count = 0;
        NotifyKey(0x01);
    }
#endif    
    //to do if any ...
    //add user sending data notify operation ....

}

void UsrProcCallback(u8 evt, u8* contxt) //invoked at each BB sleep tick
{
    static u8 key_pressed = 0;
    
    //TXRX_IDLE, NULL
    GPIO_ToggleBits(GPIO, GPIO_PIN_10); //led flash
    
    if(GPIO_ReadInputDataBit(GPIO, GPIO_PIN_0)){
        key_pressed ++;
    }else{
        key_pressed = 0;
    }
    if(10 == key_pressed){
        NotifyKey(0x01);
        key_pressed = 0;
        GPIO_ToggleBits(GPIO, GPIO_PIN_10); //led flash
    }
}

void MgBle_ConnStatusUpdate(u8 status /*PERIPH_CONNECT_XXXXX*/) //porting api
{
    switch(status)
    {
        case PERIPH_CONNECT_CONNECTED:
            CanNotifyFlag = 0; //reset
            break;
        
        case PERIPH_CONNECT_DISCONNECTED:
            
            break;
        
        default:break; //error
    }
}

void server_blob_rd_rsp(u8 attOpcode, u16 attHandle, u8 dataHdrP, u16 offset)
{
    u16 size;
    
    if(attHandle == 0x22)//hid report map
    {
        if(offset + 0x16 <= HID_MAP_SIZE)size = 0x16;
        else size = HID_MAP_SIZE - offset;
        att_server_rd( dataHdrP, attOpcode, attHandle, (u8*)(Hid_map+offset), size);
        
        return;
    }
    
    att_notFd( dataHdrP, attOpcode, attHandle);/*the default response, also for the purpose of error robust */
}


//return 1 means found
su32 GetPrimaryServiceHandle(u16 hd_start, u16 hd_end,
                            u16 uuid16,   
                            u16* hd_start_r,u16* hd_end_r)
{

    if((uuid16 == 0x1812) && (hd_start <= 0x19))// MUST keep match with the information saved in function  att_server_rdByGrType(...)
    {
        *hd_start_r = 0x19;
        *hd_end_r = 0x2a;
        return 1;
    }
    
    return 0;
}
