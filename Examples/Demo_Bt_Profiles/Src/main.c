/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include <stdio.h>
#include "mg_driver.h"


void test_ble_stack_slave_profiles(void);

static u32 mydelay(int num)
{
    u32 i,j=0;
    
    for(i = 0 ; i < num ; i ++)
    {
        j += i;
    }
    
    return j;
}

int main(void)
{
    mydelay(0xa00000*3);

    NVIC_EnableIRQ(INT_MASK_BIT_WATCHDOG);    
    WDog_Init(5*32768);  //irq enabled, clock div = 1; clk:32768Hz

    test_ble_stack_slave_profiles();

    while(1);  //never be here
}
