/**
  ******************************************************************************
  * @file    mg_test_ble.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"
#include "mg_ble_control.h"



void toggle_led(void)
{
    static char high = 0;
    
    if(high)GPIO->DATA_OUT_SET = GPIO_PIN_10;
    else GPIO->DATA_OUT_CLEAR = GPIO_PIN_10;
    
    high = !high;
}

void init_led(void)
{
    GPIO_InitTypeDef cfg;
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A10,GPIO_MAP_PIN_FUNC3);
    
    cfg.GPIO_Mode = GPIO_Mode_Out_PP;
    cfg.GPIO_Pin  = GPIO_PIN_10;
    GPIO_Init(GPIO, &cfg);

    GPIO_ResetBits(GPIO, GPIO_PIN_10); //L = led_on
}

////// !!! porting function, this will be invoked by ble lib. !!!/////
void slave_app_cfg(void)
{
    u8 adv[] = {2,0x01,6, 11,0x09,'m','g','-','p','r','o','f','i','l','e'};
    u8 rsp[] = {5,0xff,'m','g','1','7'};

#if 0
    //PROFILE_HRS_ENABLED
    const u8 adv[] = //AdvDat_HRS[]=
    {
        0x02,0x01,0x06,
        0x03,0x19,0x41,0x03,
        0x07,0x03,0x0D,0x18,0x0A,0x18,0x0F,0x18
    };    
#endif
    
    BLE_ADV_DATA_INFO slave_cfg;
    
    slave_cfg.advType = ADV_IND;
    slave_cfg.advData = adv;
    slave_cfg.advDataLen = sizeof(adv);    
    slave_cfg.scanResponseData= rsp;    
    slave_cfg.scanResponseDataLen = sizeof(rsp);
    slave_cfg.interval = 160; //100ms
    slave_cfg.txAddrType = 1;
    slave_cfg.directIndTargetAddr[0] = 0xaa; //target addr if any
    slave_cfg.directIndTargetAddr[1] = 0xbb;
    slave_cfg.directIndTargetAddrType = 1;

    MgBle_Periph_Init(&slave_cfg);
}

void test_ble_stack_slave_profiles(void)
{
    u8 *ble_addr;
    
    init_led(); //debug purpose ONLY
    UART_Set_baudrate(115200);
    
    //================== BLE Base band init ================
    MgBle_Init(TXPWR_0DBM, &ble_addr);
    
    //============ clear MCU pending irq if any ============
    NVIC_IrqClearPendingIRQ(BBSeg_IRQn);
    NVIC_IrqClearPendingIRQ(BB_IRQn);
    
    //============== Enable NVIC IRQ bits ==================
    NVIC_EnableIRQ(INT_MASK_BIT_BB_SEG);
    NVIC_EnableIRQ(INT_MASK_BIT_BB);
    
    //================== BLE Slave role init ===============
    slave_app_cfg(); //cfg the slave
    
    //================ BLE Slave stack running =============
    while(1)
    {
        //*** user task inserted here ***
        //to do...
        
        SysGotoStopMode(); //MCU goto power saving mode
    }
    
    //never be here
}
