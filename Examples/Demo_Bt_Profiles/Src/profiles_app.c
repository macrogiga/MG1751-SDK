/**
  ******************************************************************************
  * @file    profiles_app.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include <string.h>
#include "mg_driver.h"
#include "mg_ble_control.h"



#define PROFILE_UART_ENABLED /*nRf UART*/
#define PROFILE_HRS_ENABLED  /*HRP*/
#define PROFILE_BAS_ENABLED  /*battery service*/
#define PROFILE_IPS_ENABLED  /*indoor position service*/
#define PROFILE_WSP_ENABLED  /*weight scale*/
#define PROFILE_IAS_ENABLED  /*alert*/
#define PROFILE_TPS_ENABLED  /*TX Power*/
#define PROFILE_LAS_ENABLED  /*Link Loss alert*/


#define HD_UART_BASE 0x20
#define HD_HRS_BASE  0x30
#define HD_BAS_BASE  0x40
#define HD_IPS_BASE  0x50
#define HD_WSP_BASE  0x60
#define HD_IAS_BASE  0x70
#define HD_TPS_BASE  0x80
#define HD_LAS_BASE  0x90

///////////////////////////////////////////////////////////////////////////////////////////////
void ser_write_rsp_pkt(u8 pdu_type); /*if the related character has the property of WRITE(with response) or TYPE_CFG, one MUST invoke this func*/
void att_notFd(u8 pdu_type, u8 attOpcode, u16 attHd );
void att_server_rdByGrTypeRspDeviceInfo(u8 pdu_type);
void att_server_rdByGrTypeRspPrimaryService(u8 pdu_type, u16 start_hd, u16 end_hd, u8*uuid, u8 uuidlen);
void att_server_rd( u8 pdu_type, u8 attOpcode, u16 att_hd, u8* attValue, u8 datalen );


#define UUID16_FORMAT  0xff


#define SOFTWARE_INFO "SV2.0.1"
#define MANU_INFO     "MacroGiga Bluetooth"
#define DeviceInfo    "mg1751"  /*max len is 24 bytes*/

extern u8 StartEncryption; //defined in lib
u16 cur_notifyhandle = 0x12;  //Note: make sure each notify handle by invoking function: set_notifyhandle(hd);

u8* getDeviceInfoData(u8* len)
{    
    *len = sizeof(DeviceInfo);
    return (u8*)DeviceInfo;
}

u8* getsoftwareversion(void)
{
    return (u8*)SOFTWARE_INFO;
}

/**********************************************************************************
                 *****DataBase****

01 - 06  GAP (Primary service) 0x1800
  03:04  name
07 - 0f  Device Info (Primary service) 0x180a
  0a:0b  firmware version
  0e:0f  software version
************************************************************************************/

typedef struct ble_character16{
    u16 type16;          //type2
    u16 handle_rec;      //handle
    u8  characterInfo[5];//property1 - handle2 - uuid2
    u8  uuid128_idx;     //0xff means uuid16,other is idx of uuid128
}BLE_CHAR;

typedef struct ble_UUID128{
    u8  uuid128[16];//uuid128 string: little endian
}BLE_UUID128;

//
///STEP0:Character declare
//
const BLE_CHAR AttCharList[] = {
// ======  gatt =====  Do NOT Change!!
    {TYPE_CHAR,0x03,ATT_CHAR_PROP_RD, 0x04,0,0x00,0x2a,UUID16_FORMAT},//name
    //05-06 reserved
// ======  device info =====    Do NOT Change if using the default!!!  
    {TYPE_CHAR,0x08,ATT_CHAR_PROP_RD, 0x09,0,0x29,0x2a,UUID16_FORMAT},//manufacture
    {TYPE_CHAR,0x0a,ATT_CHAR_PROP_RD, 0x0b,0,0x26,0x2a,UUID16_FORMAT},//firmware version
    {TYPE_CHAR,0x0e,ATT_CHAR_PROP_RD, 0x0f,0,0x28,0x2a,UUID16_FORMAT},//sw version
    
// ====== UART service if any ======  handle base is HD_UART_BASE
#ifdef PROFILE_UART_ENABLED
    // ======  User service or other services added here =====  User defined  
    {TYPE_CHAR,HD_UART_BASE+1, {ATT_CHAR_PROP_NTF,                     HD_UART_BASE+2,0, 0,0}, 1/*uuid128-idx1*/ },//RxNotify
    {TYPE_CFG, HD_UART_BASE+3, {ATT_CHAR_PROP_RD|ATT_CHAR_PROP_W}},//cfg    
    {TYPE_CHAR,HD_UART_BASE+4, {ATT_CHAR_PROP_W|ATT_CHAR_PROP_W_NORSP, HD_UART_BASE+5,0, 0,0}, 2/*uuid128-idx2*/ },//Tx    
    {TYPE_CHAR,HD_UART_BASE+7, {ATT_CHAR_PROP_W|ATT_CHAR_PROP_RD,      HD_UART_BASE+8,0, 0,0}, 3/*uuid128-idx3*/ },//BaudRate
    {TYPE_INFO,HD_UART_BASE+9, {ATT_CHAR_PROP_RD}},//description,"BaudRate"
#endif

#ifdef PROFILE_HRS_ENABLED
    {TYPE_CHAR,HD_HRS_BASE+1,ATT_CHAR_PROP_NTF, HD_HRS_BASE+2,0, 0x37,0x2A, UUID16_FORMAT},//HRM value
    {TYPE_CFG, HD_HRS_BASE+3,ATT_CHAR_PROP_RD|ATT_CHAR_PROP_W},//cfg
    {TYPE_CHAR,HD_HRS_BASE+4,ATT_CHAR_PROP_RD,  HD_HRS_BASE+5,0, 0x38,0x2A, UUID16_FORMAT},
#endif
    
#ifdef PROFILE_BAS_ENABLED
    {TYPE_CHAR,HD_BAS_BASE+1,ATT_CHAR_PROP_RD|ATT_CHAR_PROP_NTF,    HD_BAS_BASE+2,0,0x19,0x2a,UUID16_FORMAT},//battery level
    {TYPE_CFG, HD_BAS_BASE+3,ATT_CHAR_PROP_RD|ATT_CHAR_PROP_W},//cfg
#endif
    
#ifdef PROFILE_IPS_ENABLED
    {TYPE_CHAR,HD_IPS_BASE+1,ATT_CHAR_PROP_RD,    HD_IPS_BASE+2,0,0xad,0x2A,UUID16_FORMAT},//Indoor Positioning Configuration
    {TYPE_INFO,HD_IPS_BASE+3,ATT_CHAR_PROP_RD},
#endif
    
#ifdef PROFILE_WSP_ENABLED
    {TYPE_CHAR,HD_WSP_BASE+1,ATT_CHAR_PROP_RD,    HD_WSP_BASE+2,0,0x9e,0x2A,UUID16_FORMAT},//Weight Scale Feature
    {TYPE_INFO,HD_WSP_BASE+3,ATT_CHAR_PROP_RD},
    {TYPE_CHAR,HD_WSP_BASE+4,ATT_CHAR_PROP_IND,   HD_WSP_BASE+5,0,0x9d,0x2A,UUID16_FORMAT},//Weight Measurement
    {TYPE_CFG, HD_WSP_BASE+6,ATT_CHAR_PROP_RD|ATT_CHAR_PROP_W},//cfg
    {TYPE_INFO,HD_WSP_BASE+7,ATT_CHAR_PROP_RD},
#endif
    
#ifdef PROFILE_IAS_ENABLED
     {TYPE_CHAR,HD_IAS_BASE+1,ATT_CHAR_PROP_W_NORSP, HD_IAS_BASE+2,0,0x06,0x2A,UUID16_FORMAT}, //Alert Level
#endif
     
#ifdef PROFILE_TPS_ENABLED
    {TYPE_CHAR,HD_TPS_BASE+1,ATT_CHAR_PROP_RD, HD_TPS_BASE+2,0,0x07,0x2A,UUID16_FORMAT}, //tx power
#endif
    
#ifdef PROFILE_LAS_ENABLED // ======  Link Loss =====
    {TYPE_CHAR,HD_LAS_BASE+1,ATT_CHAR_PROP_RD|ATT_CHAR_PROP_W, HD_LAS_BASE+2,0,0x06,0x2A,UUID16_FORMAT}, //Alert Level
#endif
};

const BLE_UUID128 AttUuid128List[] = {
    /*for supporting the android app [nRF UART V2.0], one SHOULD using the 0x9e,0xca,0xdc.... uuid128*/
    {0x9e,0xca,0xdc,0x24,0x0e,0xe5,0xa9,0xe0,0x93,0xf3,0xa3,0xb5,1,0,0x40,0x6e}, //idx0,little endian, service uuid
    {0x9e,0xca,0xdc,0x24,0x0e,0xe5,0xa9,0xe0,0x93,0xf3,0xa3,0xb5,3,0,0x40,0x6e}, //idx1,little endian, Rx-Notify
    {0x9e,0xca,0xdc,0x24,0x0e,0xe5,0xa9,0xe0,0x93,0xf3,0xa3,0xb5,2,0,0x40,0x6e}, //idx2,little endian, Tx
    {0x9e,0xca,0xdc,0x24,0x0e,0xe5,0xa9,0xe0,0x93,0xf3,0xa3,0xb5,4,0,0x40,0x6e}, //idx3,little endian, BaudRate
};

u8 GetCharListDim(void)
{
    return sizeof(AttCharList)/sizeof(AttCharList[0]);
}

//////////////////////////////////////////////////////////////////////////
///STEP1:Service declare
// read by type request handle, primary service declare implementation
void att_server_rdByGrType( u8 pdu_type, u8 attOpcode, u16 st_hd, u16 end_hd, u16 att_type )
{
 //!!!!!!!!  hard code for gap and gatt, make sure here is 100% matched with database:[AttCharList] !!!!!!!!!

    if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd == 1))//hard code for device info service
    {
        att_server_rdByGrTypeRspDeviceInfo(pdu_type);//using the default device info service
        //apply user defined (device info)service example
        //{
        //    u8 t[] = {0xa,0x18};
        //    att_server_rdByGrTypeRspPrimaryService(pdu_type,0x7,0xf,(u8*)(t),2);
        //}
        return;
    }
#ifdef PROFILE_UART_ENABLED
    else if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd <= HD_UART_BASE)) //usr's service
    {
        att_server_rdByGrTypeRspPrimaryService(pdu_type,HD_UART_BASE,HD_UART_BASE+9,(u8*)(AttUuid128List[0].uuid128),16);
        return;
    }
#endif
    
#ifdef PROFILE_HRS_ENABLED
    else if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd <= HD_HRS_BASE))
    {
        u8 t_hrs[]={0x0D,0x18};
        att_server_rdByGrTypeRspPrimaryService(pdu_type,HD_HRS_BASE,HD_HRS_BASE+5,(u8*)t_hrs,2);
        return;
    }
#endif
    
#ifdef PROFILE_BAS_ENABLED
    else if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd <= HD_BAS_BASE))
    {
        u8 battery[2] = {0x0f,0x18};
        att_server_rdByGrTypeRspPrimaryService(pdu_type,HD_BAS_BASE,HD_BAS_BASE+3,(u8*)(battery),2);
        return;
    }
#endif
    
#ifdef PROFILE_IPS_ENABLED
    else if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd <= HD_IPS_BASE))
    {
        u8 s_ips[2] = {0x21,0x18};
        att_server_rdByGrTypeRspPrimaryService(pdu_type,HD_IPS_BASE,HD_IPS_BASE+3,(u8*)(s_ips),2);
        return;
    }
#endif
    
#ifdef PROFILE_WSP_ENABLED
    else if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd <= HD_WSP_BASE))
    {
        u8 s_wsp[2] = {0x1d,0x18};
        att_server_rdByGrTypeRspPrimaryService(pdu_type,HD_WSP_BASE,HD_WSP_BASE+7,(u8*)(s_wsp),2);
        return;
    }
#endif
    
#ifdef PROFILE_IAS_ENABLED
    else if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd <= HD_IAS_BASE))
    {
        u8 s_ias[2] = {0x02,0x18};
        att_server_rdByGrTypeRspPrimaryService(pdu_type,HD_IAS_BASE,HD_IAS_BASE+2,(u8*)(s_ias),2);
        return;
    }
#endif
    
#ifdef PROFILE_TPS_ENABLED
    else if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd <= HD_TPS_BASE))
    {
        u8 s_tp[2] = {0x04,0x18};
        att_server_rdByGrTypeRspPrimaryService(pdu_type,HD_TPS_BASE,HD_TPS_BASE+2,(u8*)(s_tp),2);
        return;
    }
#endif
    
#ifdef PROFILE_LAS_ENABLED
    else if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd <= HD_LAS_BASE))
    {
        u8 s_tp[2] = {0x03,0x18};
        att_server_rdByGrTypeRspPrimaryService(pdu_type,HD_LAS_BASE,HD_LAS_BASE+2,(u8*)(s_tp),2);
        return;
    }
#endif
    
    //other service added here if any
    //to do....

    ///error handle
    att_notFd( pdu_type, attOpcode, st_hd );
}

#ifdef PROFILE_LAS_ENABLED
u8 LL_Level = 1;
#endif

#ifdef PROFILE_UART_ENABLED
void module_data_out(u8* data, u8 len) //blocking method, one may change it to non-blocking,heihei...
{
    while(len)
    {
        UART_SendOneByte(*data);
        data ++;
        len --;
    }
}
#endif

///STEP2:data coming
///write response, data coming....
void ser_write_rsp(u8 pdu_type/*reserved*/, u8 attOpcode/*reserved*/, 
                   u16 att_hd, u8* attValue/*app data pointer*/, u8 valueLen_w/*app data size*/)
{   
    switch(att_hd)
    {    
#ifdef PROFILE_UART_ENABLED
        case HD_UART_BASE+8:
            //BaudRate setting here
            ser_write_rsp_pkt(pdu_type);
            break;
        case HD_UART_BASE+5:
            //data-in coming
            ser_write_rsp_pkt(pdu_type);
            
            //uart out incoming data, test purpose only!!
            module_data_out(attValue,valueLen_w);
        
            //loop back, test purpose only!!
            if(valueLen_w > 24) //long frame package
            {
                sconn_notifyLongPktData(HD_UART_BASE + 2,attValue,valueLen_w);
            }
            else//short frame package
            {
                cur_notifyhandle = HD_UART_BASE + 2;
                sconn_notifydata(attValue,valueLen_w);
            }
        
            break;
        case HD_UART_BASE+3:
            //cfg
            ser_write_rsp_pkt(pdu_type);
            break;
#endif
        
#ifdef PROFILE_HRS_ENABLED
        case HD_HRS_BASE+3: //cfg
            ser_write_rsp_pkt(pdu_type);
            break;
#endif
        
#ifdef PROFILE_BAS_ENABLED
        case HD_BAS_BASE+3: //cfg
            ser_write_rsp_pkt(pdu_type);
            break;
#endif
        
#ifdef PROFILE_WSP_ENABLED
        case HD_WSP_BASE+6: //cfg
            ser_write_rsp_pkt(pdu_type);
            break;
#endif
        
#ifdef PROFILE_IAS_ENABLED
        case HD_IAS_BASE+2:
            //save the alert level cfg here , to do...
            break;
#endif

#ifdef PROFILE_LAS_ENABLED
        case HD_LAS_BASE+2:
            LL_Level = (*attValue);
            ser_write_rsp_pkt(pdu_type);
            break;
#endif

        default:
            ser_write_rsp_pkt(pdu_type);
//            att_notFd( pdu_type, attOpcode, att_hd );	/*the default response, also for the purpose of error robust */
            break;
    }
 }

 ///STEP2.1:Queued Writes data if any
void ser_prepare_write(u16 handle, u8* attValue, u16 attValueLen, u16 att_offset)//user's call back api 
{
    //queued data:offset + data(size)
    //when ser_execute_write() is invoked, means end of queue write.
    
    //to do
}
 
void ser_execute_write(void)//user's call back api 
{
    //end of queued writes  
    
    //to do...
}

///STEP3:Read data
//// read response
void server_rd_rsp(u8 attOpcode, u16 attHandle, u8 pdu_type)
{
    switch(attHandle) //hard code
    {
        case 0x04: //name
            att_server_rd( pdu_type, attOpcode, attHandle, (u8*)(DeviceInfo), sizeof(DeviceInfo)-1);
            break;
        
        case 0x09: //MANU_INFO
            //att_server_rd( pdu_type, attOpcode, attHandle, (u8*)(MANU_INFO), sizeof(MANU_INFO)-1);
            att_server_rd( pdu_type, attOpcode, attHandle, MgBle_GetBleStackVersion(), strlen((const char*)MgBle_GetBleStackVersion())); //ble lib build version
        
            break;
        
        case 0x0b: //FIRMWARE_INFO
        {
            //do NOT modify this code!!!
            att_server_rd( pdu_type, attOpcode, attHandle, GetFirmwareInfo(),strlen((const char*)GetFirmwareInfo()));
            break;
        }
        
        case 0x0f://SOFTWARE_INFO
            att_server_rd( pdu_type, attOpcode, attHandle, (u8*)(SOFTWARE_INFO), sizeof(SOFTWARE_INFO)-1);
            break;
        
#ifdef PROFILE_UART_ENABLED
        case HD_UART_BASE+3:
            //cfg
            {
                u8 t[2] = {0,0};
                att_server_rd( pdu_type, attOpcode, attHandle, t, 2);
            }
            break;
        case HD_UART_BASE+8:
            {
                u8 tab[3]; //11520
                tab[0]=0x01;
                tab[1]=0xc2;
                tab[2]=0x00;
                att_server_rd( pdu_type, attOpcode, attHandle, tab, 3);
            }
            break;
        case HD_UART_BASE+9:
             #define MG_BaudRate "BaudRate"
            att_server_rd( pdu_type, attOpcode, attHandle, (u8*)(MG_BaudRate), sizeof(MG_BaudRate)-1);
            break;
#endif
        
#ifdef PROFILE_HRS_ENABLED
        case HD_HRS_BASE+3:
        //cfg
            {
                u8 t[2] = {0,0};
                att_server_rd( pdu_type, attOpcode, attHandle, t, 2);
            }
            break;
        case HD_HRS_BASE+5:
            {
                u8 Type[1] = {0x03};//finger
                att_server_rd( pdu_type, attOpcode, attHandle, Type, 1);
            }
            
            break;
#endif
         
#ifdef PROFILE_BAS_ENABLED
        case HD_BAS_BASE+2:
        {
            u8 t[1] = {90}; //battery level 90%
            att_server_rd( pdu_type, attOpcode, attHandle, t, 1);
        }
            break;  
#endif
        
#ifdef PROFILE_IPS_ENABLED
        case HD_IPS_BASE+2://Indoor Positioning Configuration
            att_server_rd( pdu_type, attOpcode, attHandle, (u8*)"  ", 1);
            break;
        case HD_IPS_BASE+3://Indoor Positioning Configuration info
            att_server_rd( pdu_type, attOpcode, attHandle, (u8*)"Indoor", 6);
            break;
#endif
        
#ifdef PROFILE_WSP_ENABLED
        case HD_WSP_BASE+2: //feature
            {
                u8 scale_feature[4] = {0x7F,0,0,0};
                att_server_rd( pdu_type, attOpcode, attHandle,scale_feature, 4);
            }
            break;
        case HD_WSP_BASE+3:
            att_server_rd( pdu_type, attOpcode, attHandle, (u8*)"Feature", 7);
            break;
        case HD_WSP_BASE+6:
            {
                u8 t[2] = {0,0};
                att_server_rd( pdu_type, attOpcode, attHandle, t, 2);
            }
            break;
        case HD_WSP_BASE+7:
            att_server_rd( pdu_type, attOpcode, attHandle, (u8*)"Measurement", 11);
            break;
#endif
        
#ifdef PROFILE_TPS_ENABLED
        case HD_TPS_BASE+2:
        {
            u8 value[1];
            value[0] = 0; //0dbm
            att_server_rd( pdu_type, attOpcode, attHandle, (u8*)value, 1);
        }
        break;
#endif

#ifdef PROFILE_LAS_ENABLED
        case HD_LAS_BASE+2:
            att_server_rd( pdu_type, attOpcode, attHandle, (u8*)(&LL_Level), 1);            
            break;
#endif
        
        default:
            att_notFd( pdu_type, attOpcode, attHandle );/*the default response, also for the purpose of error robust */
            break;
    }
}

//本回调函数可用于蓝牙模块端主动发送数据之用，协议栈会在系统允许的时候（异步）回调本函数，不得阻塞！！
void Gatt_UserSendNotify(void)
{
#ifdef PROFILE_HRS_ENABLED
    {
        u8 data[20];
        u8 len;
        static u8 counter = 50;
        
        #define HR_BIT8  0x00
        #define HR_BIT16 0x01
        #define SENSOR_BIT 0x04
        #define SENSOR_OK  0x02
        #define ENERGY_BIT 0x08
        #define RR_INTERVAL_BIT 0x10
        
        counter --;
        if(counter)goto END_HRS;
        counter = 50;
        
        len = 1;
        data[0] = HR_BIT8;//flag
        data[len++] = 70;//HR
        
        data[0] |= (SENSOR_BIT|SENSOR_OK);

        data[0] |= ENERGY_BIT;
        data[len++] = 60;//energy
        data[len++] = 0;
        
        data[0] |= RR_INTERVAL_BIT;
        data[len++] = 17;
        data[len++] = 0;
        
        cur_notifyhandle = HD_HRS_BASE+2;
        if(1/*cfg_enabled*/)
        {
            sconn_notifydata(data,len);
            
            //for other format if supported, then
            #if 0
            data[0] |= (SENSOR_BIT|SENSOR_OK);
            sconn_notifydata(data,len);
            
            len = 1;
            data[0] = HR_BIT16;//flag
            data[len++] = 6;//HR
            data[len++] = 1;
            sconn_notifydata(data,len);
            #endif
        }
    }
    END_HRS:;
#endif
    
#ifdef PROFILE_WSP_ENABLED
    {
        u8 data[4];
        static u8 ws_counter = 50;
        u16 weight = 10020; //100.20 pounds

        ws_counter --;
        if(ws_counter)goto END_WSP;
        ws_counter = 50;
        
        data[0] = 0x01;//flag
        data[1] = weight&0xff;
        data[2] = weight>>8;
        
        cur_notifyhandle = HD_WSP_BASE+5;
        sconn_indicationdata(data,3);
    }
    END_WSP:;
#endif
    
    //to do if any ...
    //add user sending data notify operation ....
    
}

void UsrProcCallback(u8 evt, u8* contxt) //invoked at each BB sleep tick
{
    //TXRX_IDLE, NULL
}

void MgBle_ConnStatusUpdate(u8 status /*PERIPH_CONNECT_XXXXX*/) //porting api
{
    switch(status)
    {
        case PERIPH_CONNECT_CONNECTED:
            break;
        
        case PERIPH_CONNECT_DISCONNECTED:
            break;
        
        default:break; //error
    }
}

void server_blob_rd_rsp(u8 attOpcode, u16 attHandle, u8 dataHdrP, u16 offset)
{
//    u16 size;
//    
//    if(attHandle == 0x22)//hid report map
//    {
//        if(offset + 0x16 <= HID_MAP_SIZE)size = 0x16;
//        else size = HID_MAP_SIZE - offset;
//        att_server_rd( dataHdrP, attOpcode, attHandle, (u8*)(Hid_map+offset), size);
//        
//        return;
//    }
//    
    att_notFd( dataHdrP, attOpcode, attHandle);/*the default response, also for the purpose of error robust */
}


//return 1 means found
su32 GetPrimaryServiceHandle(u16 hd_start, u16 hd_end,
                            u16 uuid16,   
                            u16* hd_start_r,u16* hd_end_r)
{
// example
//    if((uuid16 == 0x1812) && (hd_start <= 0x19))// MUST keep match with the information saved in function  att_server_rdByGrType(...)
//    {
//        *hd_start_r = 0x19;
//        *hd_end_r = 0x2a;
//        return 1;
//    }
    
#ifdef PROFILE_HRS_ENABLED
    if((uuid16 == 0x180d) && (hd_start <= HD_HRS_BASE+5))// MUST keep match with the information saved in function  att_server_rdByGrType(...)
    {
        *hd_start_r = HD_HRS_BASE;
        *hd_end_r = HD_HRS_BASE+5;
        return 1;
    }
#endif
    
#ifdef PROFILE_BAS_ENABLED
    if((uuid16 == 0x180f) && (hd_start <= HD_BAS_BASE+3))
    {
        *hd_start_r = HD_BAS_BASE;
        *hd_end_r = HD_BAS_BASE+3;
        return 1;
    }
#endif

#ifdef PROFILE_IPS_ENABLED
    if((uuid16 == 0x1821) && (hd_start <= HD_IPS_BASE+3))
    {
        *hd_start_r = HD_IPS_BASE;
        *hd_end_r = HD_IPS_BASE+3;
        return 1;
    }
#endif
    
#ifdef PROFILE_WSP_ENABLED
    if((uuid16 == 0x181d) && (hd_start <= HD_WSP_BASE+7))
    {
        *hd_start_r = HD_WSP_BASE;
        *hd_end_r = HD_WSP_BASE+7;
        return 1;
    }
#endif
    
#ifdef PROFILE_IAS_ENABLED
    if((uuid16 == 0x1802) && (hd_start <= HD_IAS_BASE+2))
    {
        *hd_start_r = HD_IAS_BASE;
        *hd_end_r = HD_IAS_BASE+2;
        return 1;
    }
#endif
    
#ifdef PROFILE_TPS_ENABLED 
    if((uuid16 == 0x1804) && (hd_start <= HD_TPS_BASE+2))
    {
        *hd_start_r = HD_TPS_BASE;
        *hd_end_r = HD_TPS_BASE+2;
        return 1;
    }
#endif
    
#ifdef PROFILE_LAS_ENABLED
    if((uuid16 == 0x1803) && (hd_start <= HD_LAS_BASE+2))
    {
        *hd_start_r = HD_LAS_BASE;
        *hd_end_r = HD_LAS_BASE+2;
        return 1;
    }
#endif
    
    return 0;
}
