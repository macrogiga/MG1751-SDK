following types of service are supported in this demo prj.

#define PROFILE_UART_ENABLED /*nRf UART*/
#define PROFILE_HRS_ENABLED  /*HRP*/
#define PROFILE_BAS_ENABLED  /*battery service*/
#define PROFILE_IPS_ENABLED  /*indoor position service*/
#define PROFILE_WSP_ENABLED  /*weight scale*/
#define PROFILE_IAS_ENABLED  /*alert*/
#define PROFILE_TPS_ENABLED  /*TX Power*/
#define PROFILE_LAS_ENABLED  /*Link Loss alert*/

