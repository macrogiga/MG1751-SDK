/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2021 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include <stdio.h>
#include "mg_driver.h"
#include "mg_ble_control.h"


extern void test_ble_slave(void);
extern void set_slave_role_timeout(void);
extern void test_ble_master(void);
extern void set_master_role_timeout(void);


static u8 cur_role = ROLE_SLAVE;

u8 get_cur_role(void)
{
    return cur_role;
}

void set_cur_role(u8 role)
{
    cur_role = role;
}

void PA_IO_init(void)
{
    GPIO_InitTypeDef init;
    u32 delay = 0x200000;
    
    while(delay--); //for SWD ports' safe usage ONLY!!!
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A7,GPIO_MAP_PIN_FUNC3);
    GPIO_FunctionSelect(GPIO_MAP_PIN_A8,GPIO_MAP_PIN_FUNC3);
    
    init.GPIO_Pin = GPIO_PIN_7 | GPIO_PIN_8;
    init.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIO, &init);
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A7,GPIO_MAP_PIN_FUNC2);/*TX*/
    GPIO_FunctionSelect(GPIO_MAP_PIN_A8,GPIO_MAP_PIN_FUNC2);/*RX*/
}

void toggle_led_io(void)
{
    static u8 high = 0;
    
    if(high)GPIO->DATA_OUT_SET = GPIO_PIN_10;
    else GPIO->DATA_OUT_CLEAR = GPIO_PIN_10;
    
    high = !high;
}

void led_io_init(void)
{
    GPIO_InitTypeDef cfg;
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A10,GPIO_MAP_PIN_FUNC3);
    
    cfg.GPIO_Mode = GPIO_Mode_Out_PP;
    cfg.GPIO_Pin  = GPIO_PIN_10;
    GPIO_Init(GPIO, &cfg);
    
    GPIO->DATA_OUT_CLEAR = GPIO_PIN_10;
}

void key_io_init(void)
{
    GPIO_InitTypeDef cfg;
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A0,GPIO_MAP_PIN_FUNC1);
    
    cfg.GPIO_Mode = GPIO_Mode_IPD;
    cfg.GPIO_Pin  = GPIO_PIN_0;
    GPIO_Init(GPIO, &cfg);
}

u32 mydelay(u32 num)
{
    u32 i,j=0;
    
    for(i = 0 ; i < num ; i ++)
    {
        j += i;
    }
    
    return j;
}

int main(void)
{
    u8 *ble_addr;
    
    mydelay(0x100000);
//    MgBle_Dbg_Spi_init(); //enable dbg spi port,A3(cs),A4(clk),A9(MI),A15(MO)
    
    NVIC_EnableIRQ(INT_MASK_BIT_WATCHDOG);
    WDog_Init(5*32768);  //irq enabled, clock div = 1; clk:32768Hz  
    
    led_io_init(); //debug purpose ONLY
    UART_Set_baudrate(115200);

    //================== BLE Base band init ================
    MgBle_Init(TXPWR_0DBM, &ble_addr);//
    
    //============ clear MCU pending irq if any ============
    NVIC_IrqClearPendingIRQ(BBSeg_IRQn);
    NVIC_IrqClearPendingIRQ(BB_IRQn);
    
    //============== Enable NVIC IRQ bits ==================
    NVIC_EnableIRQ(INT_MASK_BIT_BB_SEG);
    NVIC_EnableIRQ(INT_MASK_BIT_BB);
    
    while(1)
    {
        set_cur_role(ROLE_SLAVE);
        set_slave_role_timeout();
        test_ble_slave();
        
        set_cur_role(ROLE_MASTER);
        set_master_role_timeout();
        test_ble_master();
    }

}
