/**
  ******************************************************************************
  * @file    mg_test_ble.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"
#include "mg_ble_control.h"


extern u8 get_cur_role(void);
extern void uart_show_info(u8* data, u8 len);


////// !!! porting function, this will be invoked by ble lib. !!!/////
void slave_app_cfg(void)
{
    u8 adv[] = {2,1,6, 7,9,'M','G','1','7','5','1'};
    u8 rsp[] = {5,0xff,'m','g','1','7'};
    
    BLE_ADV_DATA_INFO slave_cfg;
    
    if(ROLE_SLAVE != get_cur_role())return;
    
    slave_cfg.advType = ADV_IND;//ADV_NONCONN_IND;//ADV_IND;//ADV_SCAN_IND//ADV_DIRECT_IND
    slave_cfg.advData = adv;
    slave_cfg.advDataLen = sizeof(adv);    
    slave_cfg.scanResponseData= rsp;    
    slave_cfg.scanResponseDataLen = sizeof(rsp);
    slave_cfg.interval = 160; //100ms
    slave_cfg.txAddrType = ADDR_TYPE_RANDOM;
    if(slave_cfg.advType == ADV_DIRECT_IND)
    {
        slave_cfg.directIndTargetAddr[0] = 0xaa; //target addr if any
        slave_cfg.directIndTargetAddr[1] = 0xbb;
        slave_cfg.directIndTargetAddrType = 1;
    }
    MgBle_Periph_Init(&slave_cfg);
}

void test_ble_slave(void)
{
    uart_show_info("start role slave...",19);
    
    //================== BLE Slave role init ===============
    slave_app_cfg(); //cfg the slave
    
    //================ BLE Slave stack running =============
    while(ROLE_SLAVE == get_cur_role())
    {
        //*** user task inserted here ***
        //to do...
        
        SysGotoStopMode(); //MCU goto power saving mode
    }
    
    //never be here
}
