/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CONFIG_H
#define __CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

//PA5: Uart_Tx   PA6: Uart_Rx   PA2: wakeup(i)  PA1: REQ(o)

//PA1: I2C_SCL   PA2: I2C_SDA   PA6: wakeup(i)  PA5: REQ(o)

 /*--------------------------------------------\
 |                 PORT      ATMODE     BAUD   |
 | ATCMD, 115200     H         H         H     |
 | ATCMD, 9600       H         H         L     |
 | Uart,  115200     H         L         H     |
 | Uart,  9600       H         L         L     |
 | I2C               L         -         -     |
 \--------------------------------------------*/
//input 
#define BAUD_PIN        GPIO_PIN_15
#define BAUD_MAP        GPIO_MAP_PIN_A15
#define BAUD_FUN        GPIO_MAP_PIN_FUNC1

#define ATMODE_PIN      GPIO_PIN_4
#define ATMODE_MAP      GPIO_MAP_PIN_A4
#define ATMODE_FUN      GPIO_MAP_PIN_FUNC1

#define PORT_PIN        GPIO_PIN_3
#define PORT_MAP        GPIO_MAP_PIN_A3
#define PORT_FUN        GPIO_MAP_PIN_FUNC1

//output
#define CONN_PIN        GPIO_PIN_9
#define CONN_MAP        GPIO_MAP_PIN_A9
#define CONN_FUN        GPIO_MAP_PIN_FUNC1

#define STANDBY_PIN     GPIO_PIN_11
#define STANDBY_MAP     GPIO_MAP_PIN_A11
#define STANDBY_FUN     GPIO_MAP_PIN_FUNC3


#define MAX_SIZE 490


#ifdef __cplusplus
}
#endif

#endif /* __BSP_H */
