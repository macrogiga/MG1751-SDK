/**
  ******************************************************************************
  * @file    slave_app.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include <string.h>
#include "mg_driver.h"
#include "mg_ble_control.h"
#include "Config.h"

extern u8 PosW, txLen,txBuf[];
extern unsigned char AtcmdFlag;
extern unsigned char UseUart;

extern void at_blesend(void);
extern void moduleOutData(u8*data, u8 len);
extern void CheckAtCmdInfo(void);
extern void CheckComPortInData(void);

///////////////////////////////////////////////////////////////////////////////////////////////
void ser_write_rsp_pkt(u8 pdu_type); /*if the related character has the property of WRITE(with response) or TYPE_CFG, one MUST invoke this func*/
void att_notFd(u8 pdu_type, u8 attOpcode, u16 attHd );
void att_server_rdByGrTypeRspDeviceInfo(u8 pdu_type);
void att_server_rdByGrTypeRspPrimaryService(u8 pdu_type, u16 start_hd, u16 end_hd, u8*uuid, u8 uuidlen);
void att_server_rd( u8 pdu_type, u8 attOpcode, u16 att_hd, u8* attValue, u8 datalen );


#define UUID16_FORMAT  0xff


#define SOFTWARE_INFO "SV2.0.1"
//#define MANU_INFO     "MacroGiga Bluetooth"
#define MAX_NAME_LEN  20
char DeviceInfo[MAX_NAME_LEN+1] =  {6,'M','G','1','7','5','1'};  /*first byte is len, max len is 20*/

extern u8 StartEncryption; //defined in lib
u16 cur_notifyhandle = 0x12;  //Note: make sure each notify handle by invoking function: set_notifyhandle(hd);

u8* getDeviceInfoData(u8* len)
{    
    *len = DeviceInfo[0];
    return (u8*)&DeviceInfo[1];
}


/**********************************************************************************
                 *****DataBase****

01 - 06  GAP (Primary service) 0x1800
  03:04  name
07 - 0f  Device Info (Primary service) 0x180a
  0a:0b  firmware version
  0e:0f  software version
10 - 19  LED service (Primary service) 00010203-0405-0607-0809-0a0b0c0d1910
  11:12  00010203-0405-0607-0809-0a0b0c0d1911(0x1A)  Status
  13     cfg
  14:15  00010203-0405-0607-0809-0a0b0c0d1912(0x0A)  CMD
  16     cfg
  17:18  00010203-0405-0607-0809-0a0b0c0d1913(0x0A)  OTA
  19     0x2901  info
************************************************************************************/

typedef struct ble_character16{
    u16 type16;          //type2
    u16 handle_rec;      //handle
    u8  characterInfo[5];//property1 - handle2 - uuid2
    u8  uuid128_idx;     //0xff means uuid16,other is idx of uuid128
}BLE_CHAR;

typedef struct ble_UUID128{
    u8  uuid128[16];//uuid128 string: little endian
}BLE_UUID128;

//
///STEP0:Character declare
//
const BLE_CHAR AttCharList[] = {
// ======  gatt =====  Do NOT Change!!
    {TYPE_CHAR,0x03,ATT_CHAR_PROP_RD, 0x04,0,0x00,0x2a,UUID16_FORMAT},//name
    //05-06 reserved
// ======  device info =====    Do NOT Change if using the default!!!  
    {TYPE_CHAR,0x08,ATT_CHAR_PROP_RD, 0x09,0,0x29,0x2a,UUID16_FORMAT},//manufacture
    {TYPE_CHAR,0x0a,ATT_CHAR_PROP_RD, 0x0b,0,0x26,0x2a,UUID16_FORMAT},//firmware version
    {TYPE_CHAR,0x0e,ATT_CHAR_PROP_RD, 0x0f,0,0x28,0x2a,UUID16_FORMAT},//sw version
    
// ======  LED service or other services added here =====  User defined  
    {TYPE_CHAR,0x11,ATT_CHAR_PROP_NTF, 0x12,0, 0,0, 1/*uuid128-idx1*/ },//RxNotify
    {TYPE_CFG, 0x13,ATT_CHAR_PROP_RD|ATT_CHAR_PROP_W},//cfg    
    {TYPE_CHAR,0x14,ATT_CHAR_PROP_W,                   0x15,0, 0,0, 2/*uuid128-idx2*/ },//Tx    
//    {TYPE_CHAR,0x17,ATT_CHAR_PROP_W|ATT_CHAR_PROP_RD,                   0x18,0, 0,0, 3/*uuid128-idx3*/ },//BaudRate
//    {TYPE_INFO, 0x19,ATT_CHAR_PROP_RD},//description,
};

const BLE_UUID128 AttUuid128List[] = {
    /*for supporting the android app [nRF UART V2.0], one SHOULD using the 0x9e,0xca,0xdc.... uuid128*/
    {0x9e,0xca,0xdc,0x24,0x0e,0xe5,0xa9,0xe0,0x93,0xf3,0xa3,0xb5,1,0,0x40,0x6e}, //idx0,little endian, service uuid
    {0x9e,0xca,0xdc,0x24,0x0e,0xe5,0xa9,0xe0,0x93,0xf3,0xa3,0xb5,3,0,0x40,0x6e}, //idx1,little endian, RxNotify
    {0x9e,0xca,0xdc,0x24,0x0e,0xe5,0xa9,0xe0,0x93,0xf3,0xa3,0xb5,2,0,0x40,0x6e}, //idx2,little endian, Tx
//    {0x9e,0xca,0xdc,0x24,0x0e,0xe5,0xa9,0xe0,0x93,0xf3,0xa3,0xb5,4,0,0x40,0x6e}, //idx3,little endian, BaudRate
};

u8 GetCharListDim(void)
{
    return sizeof(AttCharList)/sizeof(AttCharList[0]);
}

//////////////////////////////////////////////////////////////////////////
///STEP1:Service declare
// read by type request handle, primary service declare implementation
void att_server_rdByGrType( u8 pdu_type, u8 attOpcode, u16 st_hd, u16 end_hd, u16 att_type )
{
 //!!!!!!!!  hard code for gap and gatt, make sure here is 100% matched with database:[AttCharList] !!!!!!!!!

    if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd == 1))//hard code for device info service
    {
        att_server_rdByGrTypeRspDeviceInfo(pdu_type);//using the default device info service
        //apply user defined (device info)service example
        //{
        //    u8 t[] = {0xa,0x18};
        //    att_server_rdByGrTypeRspPrimaryService(pdu_type,0x7,0xf,(u8*)(t),2);
        //}
        return;
    }
    
    //hard code
    else if((att_type == GATT_PRIMARY_SERVICE_UUID) && (st_hd <= 0x10)) //usr's service
    {
        att_server_rdByGrTypeRspPrimaryService(pdu_type,0x10,0x19,(u8*)(AttUuid128List[0].uuid128),16);
        return;
    }
    //other service added here if any
    //to do....

    ///error handle
    att_notFd( pdu_type, attOpcode, st_hd );
}

///STEP2:data coming
///write response, data coming....
void ser_write_rsp(u8 pdu_type/*reserved*/, u8 attOpcode/*reserved*/, 
                   u16 att_hd, u8* attValue/*app data pointer*/, u8 valueLen_w/*app data size*/)
{
    unsigned char i;
    switch(att_hd)
    {
        case 0x15:
            if (UseUart)
            {
                if (AtcmdFlag)
                {
                    moduleOutData("IND:DAT=",8);
                    moduleOutData(&valueLen_w,1);
                    moduleOutData(",",1);
                }
                moduleOutData(attValue,valueLen_w);
            }
            else
            {
                if ((txLen+3+valueLen_w)<MAX_SIZE)//buff not overflow
                {
                    txBuf[txLen] = valueLen_w;
                    txBuf[txLen+1] = 0;	//dummy
                    txBuf[txLen+2] = 0;	//dummy
                    for (i=0;i<valueLen_w;i++)
                    {
                        txBuf[txLen+i+3] = *(attValue+i);
                    }
                    txLen += valueLen_w;
                    txLen += 3;
                    PosW = 0;
                    
                    GPIO_SetBits(GPIO, GPIO_PIN_5);
                }
            }
            ser_write_rsp_pkt(pdu_type);
            break;
        
        default:
            ser_write_rsp_pkt(pdu_type);
//            att_notFd( pdu_type, attOpcode, att_hd );	/*the default response, also for the purpose of error robust */
            break;
    }
 }

 ///STEP2.1:Queued Writes data if any
void ser_prepare_write(u16 handle, u8* attValue, u16 attValueLen, u16 att_offset)//user's call back api 
{
    //queued data:offset + data(size)
    //when ser_execute_write() is invoked, means end of queue write.
    unsigned char i;
    if (0x0015 == handle)
    {
        if (UseUart)
        {
            if (AtcmdFlag)
            {
                moduleOutData("IND:DAT=",8);
                moduleOutData((u8*)&attValueLen,1);
                moduleOutData(",",1);
            }
            moduleOutData(attValue,attValueLen);
        }
        else
        {
            if ((txLen+3+attValueLen)<MAX_SIZE)//buff not overflow
            {
                txBuf[txLen] = attValueLen;
                txBuf[txLen+1] = 0;	//dummy
                txBuf[txLen+2] = 0;	//dummy
                for (i=0;i<attValueLen;i++)
                {
                    txBuf[txLen+i+3] = *(attValue+i);
                }
                txLen += attValueLen;
                txLen += 3;
                PosW = 0;
                
                GPIO_SetBits(GPIO, GPIO_PIN_5);
            }
        }
    }
    //to do
}
 
void ser_execute_write(void)//user's call back api 
{
    //end of queued writes  
    
    //to do...    
}

///STEP3:Read data
//// read response
void server_rd_rsp(u8 attOpcode, u16 attHandle, u8 pdu_type)
{
    switch(attHandle) //hard code
    {
        case 0x04: //name
            att_server_rd( pdu_type, attOpcode, attHandle, (u8 *)(DeviceInfo+1), DeviceInfo[0]);
            break;
        
        case 0x09: //MANU_INFO
            //att_server_rd( pdu_type, attOpcode, attHandle, (u8*)(MANU_INFO), sizeof(MANU_INFO)-1);
            att_server_rd( pdu_type, attOpcode, attHandle, MgBle_GetBleStackVersion(), strlen((const char*)MgBle_GetBleStackVersion())); //ble lib build version
        
            break;
        
        case 0x0b: //FIRMWARE_INFO
        {            
            //do NOT modify this code!!!
            att_server_rd( pdu_type, attOpcode, attHandle, GetFirmwareInfo(),strlen((const char*)GetFirmwareInfo()));
            break;
        }
        
        case 0x0f://SOFTWARE_INFO
            att_server_rd( pdu_type, attOpcode, attHandle, (u8*)(SOFTWARE_INFO), sizeof(SOFTWARE_INFO)-1);
            break;
        
        case 0x13://cfg
            {
                u8 t[2] = {0,0};
                att_server_rd( pdu_type, attOpcode, attHandle, t, 2);
            }
            break;
            
//        case 0x19://description
//            {
//                att_server_rd( pdu_type, attOpcode, attHandle, (u8*)"BaudRate", 8);
//            }
//            break;
        
        default:
            att_notFd( pdu_type, attOpcode, attHandle );/*the default response, also for the purpose of error robust */
            break;
    }
}

//static u8 NotifyUartDataBuf[244];//max is 244

//本回调函数可用于蓝牙模块端主动发送数据之用，协议栈会在系统允许的时候（异步）回调本函数，不得阻塞！！
void Gatt_UserSendNotify(void)
{
    /*u8 NotifyUartDataLen;
    u8 max_size;
    
    //to do if any ...
    //add user sending data notify operation ....
    if(is_att_LongPktDataEnabled())
    {
        max_size = att_get_sconn_notifyLongPktMaxSize() - 4;
        NotifyUartDataLen = Get_RxFiFoData(NotifyUartDataBuf, max_size);
        if(NotifyUartDataLen >= att_get_sconn_notifyLongPktMinSize()) //long package data sending
        {
            sconn_notifyLongPktData(cur_notifyhandle,NotifyUartDataBuf,NotifyUartDataLen);
        }
        else if(NotifyUartDataLen)//short packet format sending
        {
            sconn_notifydata(NotifyUartDataBuf,NotifyUartDataLen);
        }
    }
    else //short packet case
    {
        NotifyUartDataLen = Get_RxFiFoData(NotifyUartDataBuf, 20);
        if(NotifyUartDataLen)
        {
            sconn_notifydata(NotifyUartDataBuf,NotifyUartDataLen);
        }
    }*/
}

unsigned char NotifyData(unsigned char *Buf,unsigned short Len)
{
    u8 max_size;
    if(is_att_LongPktDataEnabled())
    {
        max_size = att_get_sconn_notifyLongPktMaxSize() - 4;
        if (Len >= att_get_sconn_notifyLongPktMinSize())
        {
            if (sconn_notifyLongPktData(cur_notifyhandle,Buf,Len>max_size?max_size:Len))
            {
                return Len>max_size?max_size:Len;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return sconn_notifydata(Buf,Len>20?20:Len);
        }
    }
    else
    {
        return sconn_notifydata(Buf,Len>20?20:Len);
    }
}

static unsigned char gConnectedFlag=0;
char GetConnectedStatus(void)
{
    return gConnectedFlag;
}


void UsrProcCallback(u8 evt, u8* contxt) //invoked at each BB sleep tick
{
    static u8 cnt_disc = 0;
    
    if (AtcmdFlag)
    {
        CheckAtCmdInfo();
        at_blesend();
    }
    else
    {
        CheckComPortInData();
        if( (GetConnectedStatus())
           &&(0 == cnt_disc))
        {
            if( ((UseUart)&&(Bit_RESET == GPIO_ReadInputDataBit(GPIO, GPIO_PIN_2)))
              ||((UseUart==0)&&(Bit_RESET == GPIO_ReadInputDataBit(GPIO, GPIO_PIN_6))) )
            {
                cnt_disc = 1;
                MgBle_Periph_Disconnect();
            }
        }
        else
        {
            cnt_disc = 0;
        }
    }

    if ((txLen) && (0 == PosW))
    {
        UART_ITConfig_TX(1);
    }
}

void MgBle_ConnStatusUpdate(u8 status /*PERIPH_CONNECT_XXXXX*/) //porting api
{
    switch(status)
    {
        case PERIPH_CONNECT_CONNECTED:
            gConnectedFlag = 1;
            GPIO_SetBits(GPIO,CONN_PIN);
            if (AtcmdFlag)
                moduleOutData((u8*)"IND:CONNECT\n",12);
//            printf("connected.\n");
            break;
        
        case PERIPH_CONNECT_DISCONNECTED:
            gConnectedFlag = 0;
            GPIO_ResetBits(GPIO,CONN_PIN);
            if (AtcmdFlag)
                moduleOutData((u8*)"IND:DISCONNECT\n",15);
//            printf("disconnected.\n");
            break;
        
        default:break; //error
    }
}

void server_blob_rd_rsp(u8 attOpcode, u16 attHandle, u8 dataHdrP, u16 offset)
{
//    u16 size;
//    
//    if(attHandle == 0x22)//hid report map
//    {
//        if(offset + 0x16 <= HID_MAP_SIZE)size = 0x16;
//        else size = HID_MAP_SIZE - offset;
//        att_server_rd( dataHdrP, attOpcode, attHandle, (u8*)(Hid_map+offset), size);
//        
//        return;
//    }
//    
//    att_notFd( dataHdrP, attOpcode, attHandle);/*the default response, also for the purpose of error robust */
}


//return 1 means found
su32 GetPrimaryServiceHandle(u16 hd_start, u16 hd_end,
                            u16 uuid16,
                            u16* hd_start_r,u16* hd_end_r)
{
// example
//    if((uuid16 == 0x1812) && (hd_start <= 0x19))// MUST keep match with the information saved in function  att_server_rdByGrType(...)
//    {
//        *hd_start_r = 0x19;
//        *hd_end_r = 0x2a;
//        return 1;
//    }
    
    return 0;
}
