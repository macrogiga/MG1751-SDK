/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include <stdio.h>
#include "mg_driver.h"


extern void test_ble_iBeacon(void);

static u32 mydelay(int num)
{
    u32 i,j=0;
    
    for(i = 0 ; i < num ; i ++)
    {
        j += i;
    }
    
    return j;
}

int main(void)
{
    NVIC_EnableIRQ(INT_MASK_BIT_WATCHDOG);    
    WDog_Init(5*32768);  //irq enabled, clock div = 1; clk:32768Hz
    
    mydelay(0xa00000*3);

/**************************************************************************
    Example functionality
     - iBeacon
      - wechat UUID


    The program is a simple program that demo wechat yaoyiyao device

**************************************************************************/

    test_ble_iBeacon();

    while(1);  //never be here
}
