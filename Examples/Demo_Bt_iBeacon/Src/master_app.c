/**
  ******************************************************************************
  * @file    master_app.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"
#include "mg_ble_control.h"
#include "mg_ble_control_master.h"


u8 mconn_writedata(u16 writehandle,u8* data, u8 len);//returns bytes send

void ScanProcCallback(u8 evt, u8* contxt) //master scan callback
{
    BLE_DEV_INFO* dev = (BLE_DEV_INFO*) contxt;
    
    switch(evt)
    {
        case TXRX_IDLE:
            //add some control code here
            //to do...
        
            break;
        
        case TXRX_RX_NEW_DATA: //find device's adv data
            if(dev->adv[0])
            {
                //to do...
            }
            
            if(dev->rsp[0])
            {
                //to do...
            }

            break;
        
        default://error
            break;
    }
}

void MgBle_ConnStatusUpdate_Central(u8 Status /*CENTRAL_CONNECT_XXXXX*/)
{
    switch(Status)
    {
        case CENTRAL_CONNECT_CONNECTED:
            break;
        
        case CENTRAL_CONNECT_DISCONNECTED:
            break;
        
        case CENTRAL_CONNECT_ATTEMPT_FAILED:
            break;
        
        default:break; //error
    }
}

//BB envoked once at each BB sleep stage
void MasterProcCallback(u8 evt, u8* contxt) //master connection callback
{
    //to do...
}

//data coming from slave device(notify & indication)
void CALLBACK_Master_UsrDataOut(u16 handle, u8* data, u8 len)
{

}

//BB envoked once at each connected empty stage, one can send(write/read op) data here
void CALLBACK_Master_gatt_user_write_data(void)
{

}

//Error Response API
void CALLBACK_Master_AttError(u8 ReqOpCode,u8 *par/*handle16,reason8*/) //porting API
{   
    switch(ReqOpCode)
    {
        case ATT_FIND_BY_TYP_REQ: //find by type value req(may dedicate the primay service searching op's response)
            break;
        
        case ATT_RD_BY_TYPE_REQ:
            break;
        
        case ATT_RD_BY_GROUP_TYPE_REQ:
            break;
        
        case ATT_FIND_INFO_REQ:
            break;
        
        case ATT_RD_REQ:
            break;
        
        default:break;
    }
}

//default un-processed gatt data if any.
//ATT_FIND_BY_TYP_VALUE_RSP,ATT_RD_BY_GRP_RSP,ATT_RD_BY_TYPE_RSP...
void CALLBACK_Master_gatt_default(u8 Opcode,u8* par_data, u8 len)
{
    switch(Opcode)
    {
        case ATT_FIND_BY_TYP_RSP:
            //par_data : handle_st, handle_end
            break;
        
        case ATT_RD_BY_GROUP_TYPE_RSP:
            //par_data : group length {handle_st, handle_end, uuid}
            break;
        
        case ATT_RD_BY_TYPE_RSP:
            //par_data : group length {handle16, property8, handle16, uuid}
            break;
        
        case ATT_READ_RSP:
            //par_data : att_value
            break;
        case ATT_FIND_INFO_RSP:
            //par_data : format8, handle16, uuid 
            //format: 0x01 uuid16
            break;
        
        default:break;
    }
}
