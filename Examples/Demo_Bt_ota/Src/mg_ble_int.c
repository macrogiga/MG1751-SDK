/**
  ******************************************************************************
  * @file    mg_ble_int.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */

#include "mg_driver.h"
#include "mg_ble_control.h"
#include "mg_ble_control_master.h"

extern void toggle_led_io(void);

void UsrProcCallback(u8 evt, u8* contxt);
//void MasterProcCallback(u8 evt, u8* contxt);
//void ScanProcCallback(u8 evt, u8* contxt);


void BBSeg_IRQHandler(void) //share with the PWM_LED irq
{
    radio_seg_rx_tx_irq();
}

void BB_IRQHandler(void)
{    
    MgBle_Periph_Run(UsrProcCallback);
//    MgBle_CentralScan_Run(ScanProcCallback);
//    MgBle_CentralConnect_Run(MasterProcCallback);
    
    toggle_led_io();
    
    WDog_Feed(5*32768);
}
