/**
  ******************************************************************************
  * @file    mg_test_ble_master.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */

#include "mg_driver.h"
#include "mg_ble_control.h"
#include "mg_ble_control_master.h"


extern void led_io_init(void);

void MgBle_Master_Scan(void)
{
    MASTER_SCAN_CFG cfg;
    u8 *ble_addr;
    
    cfg.scan_interval = 160*3; //100ms = 160*0.625
    cfg.scan_window   = 160*3; //100ms = 160*0.625
    cfg.scan_type = SCAN_TYPE_PASSIVE;
    cfg.txAddrType = ADDR_TYPE_RANDOM;
    
    MgBle_Init(TXPWR_0DBM, &ble_addr);
    
    //clear pending irq if any, to do...
    NVIC_IrqClearPendingIRQ(BBSeg_IRQn);
    NVIC_IrqClearPendingIRQ(BB_IRQn);
//    NVIC_EnableIRQ(INT_MASK_BIT_BB_SEG);
    NVIC_EnableIRQ(INT_MASK_BIT_BB);
    
AGAIN: 
    MgBle_CentralScan_Init(&cfg);
    
    while(MgBle_GetCentralScanRunContinueFlag())
    {
//        SysGotoStopMode();
    }
    
    goto AGAIN; //test purpose
}

static u8 *myble_addr;

////// !!! porting function, this will be invoked by ble lib. !!!/////
void master_connect_app_cfg(void)
{
    MASTER_CONNECT_CFG cfg;
    
    cfg.targetAddrType = 0;
    cfg.txAddrType = ADDR_TYPE_RANDOM;
    cfg.attempt_num = 100;
    cfg.connect_interval = 24; /*unit 1.25ms*/

#if 0
    cfg.targetAddr[0] = 0x4C;   //4C E4 28 17 67 6D
    cfg.targetAddr[1] = 0xE4;
    cfg.targetAddr[2] = 0x28;
    cfg.targetAddr[3] = 0x17;
    cfg.targetAddr[4] = 0x67;
    cfg.targetAddr[5] = 0x6D;

#else

    cfg.targetAddr[0] = myble_addr[0];
    cfg.targetAddr[1] = myble_addr[1];
    cfg.targetAddr[2] = myble_addr[2];
    cfg.targetAddr[3] = myble_addr[3];
    cfg.targetAddr[4] = myble_addr[4];
    cfg.targetAddr[5] = myble_addr[5];
    cfg.targetAddrType = 1; //pls check this type
#endif
    
    MgBle_CentralConnect_Init(&cfg);
}

void MgBle_Master_Connect(void)
{    
    u8 *ble_addr;

    //================== BLE Base band init ================
    MgBle_Init(TXPWR_0DBM, &ble_addr);
    myble_addr = ble_addr;
    //============ clear MCU pending irq if any ============
    NVIC_IrqClearPendingIRQ(BBSeg_IRQn);
    NVIC_IrqClearPendingIRQ(BB_IRQn);
    
    //============== Enable NVIC IRQ bits ==================
    NVIC_EnableIRQ(INT_MASK_BIT_BB_SEG);
    NVIC_EnableIRQ(INT_MASK_BIT_BB);
    
    //================ BLE master connect init =============
    master_connect_app_cfg();

    //================ BLE Master stack running ============
    while(1)
    {
        //*** user task inserted here ***
        //to do...
        
        SysGotoStopMode(); //MCU goto power saving mode
    }
    
    //never be here...
}

void test_ble_stack_master(void)
{
    led_io_init(); //debug purpose ONLY
    
    UART_Set_baudrate(19200);
    
    MgBle_Master_Scan();
//    MgBle_Master_Connect();
}
