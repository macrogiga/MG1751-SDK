/**
  ******************************************************************************
  * @descrition OTA reboot Porting Function
  * @file    ota_reboot_porting.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */

#define __NVIC_PRIO_BITS          2 
#define   SysTick_IRQn           -1
#define   IRQn_Type int

#include "core_cm0.h"

void ota_ble_disconnect(void);

void sw_Reboot(void)
{
    NVIC_SystemReset();
}

void OtaSystemReboot(void)
{
    //invoking of this function means that OTA data's transferring is done! 
    //so, one may disconnect the ble and reboot later.....
    //here, I just show the result and reboot when ble disconnected.
    
    ota_ble_disconnect();
}

