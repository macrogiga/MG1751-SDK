/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"


static int my_delay(int num)
{
    int i,j=1;
    
    for(i = 0; i < num ; i ++) j+=i;
    return j;
}

int main(void)
{
    CLK_SetDiv(1);//    sysclk = 39M
    my_delay(0x00200000);
    
//    CLK_SetDiv(3);//   sysclk = 39M/3
//    my_delay(0x00200000);

    Sys_ClockOutEnable(MCO_EN_MCU_CLK_DIV4); //enable clock output, from PA10/MCO
    
//    Sys_ClockOutEnable(MCO_EN_SYS_RC_CLK); //RC clock output, from PA10/MCO
    
    while(1);
}
