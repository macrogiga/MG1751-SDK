/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"


//IO_KEY = GPIO_PIN_0
//IO_LED = GPIO_PIN_10
void init_key(void)
{
    GPIO_InitTypeDef cfg;
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A0,GPIO_MAP_PIN_FUNC1);
    
    cfg.GPIO_Mode = GPIO_Mode_IPD;
    cfg.GPIO_Pin  = GPIO_PIN_0;
    GPIO_Init(GPIO, &cfg);
}

static void init_led(void)
{
    GPIO_InitTypeDef cfg;
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A10,GPIO_MAP_PIN_FUNC3);
    
    cfg.GPIO_Mode = GPIO_Mode_Out_PP;
    cfg.GPIO_Pin  = GPIO_PIN_10;
    GPIO_Init(GPIO, &cfg);

    GPIO_ResetBits(GPIO, GPIO_PIN_10); //L = led_on
}

static int mydelay(int num)
{
    int i,j=0;
    
    for(i = 0 ; i < num ; i ++)
    {
        j += i;
    }
    
    return j;
}

int main(void)
{
    GPIO_InitTypeDef cfg;
    GPIO_EXTInt_TypeDef type;

    init_key();
    init_led();
    
    //GPA9 pin share
    GPIO_FunctionSelect(GPIO_MAP_PIN_A9,GPIO_MAP_PIN_FUNC1);
    cfg.GPIO_Mode = GPIO_Mode_IPD;
    cfg.GPIO_Pin  = GPIO_PIN_9;
    GPIO_Init(GPIO, &cfg);
    
    //rising edge irq cfg
    type.GPIO_Pin = GPIO_PIN_9;
    type.Trigger_Type = GPIO_Triger_RISING_EDGE;
    GPIO_EXTInt_Init(GPIO,&type);
    //enable gpio irq    
    NVIC_EnableIRQ(INT_MASK_BIT_GPIO);
    
    //toggle
    while(1)
    {   
        mydelay(0xA0000);      
        GPIO_ToggleBits(GPIO, GPIO_PIN_10); //led flash
        
        //read GPA0 input value if any
        if(GPIO_ReadInputDataBit(GPIO, GPIO_PIN_0)) break;
    }
    
    while(1)
    {
        GPIO_SetBits(GPIO, GPIO_PIN_10);
        mydelay(0xA0000); //L = led_off
    }
}
