/**
  ******************************************************************************
  * @file    mg_gpio_int.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */

#include "mg_driver_gpio.h"

void GPIO_IRQHandler(void)
{
    if(GPIO_PIN_9 & GPIO->INT_STATUS)
    {
        GPIO->INT_STATUS = GPIO_PIN_9; //clear irq
    }
}
