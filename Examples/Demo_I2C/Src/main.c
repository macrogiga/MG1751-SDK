/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2018 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"
#include "mg_driver_i2c.h"


unsigned char TxBuff[8] = {1,2,3,4,5,6,7,8};
unsigned char RxBuff[8];

static int my_delay(int num)
{
    int i,j=1;
    
    for(i = 0; i < num ; i ++) j+=i;
    return j;
}

static void EEPROM_Write(unsigned char WriteAddr, unsigned char * Data, unsigned char Len)
{
    I2C_SendData(WriteAddr);
    I2C_SendBuf(Data,Len);
}

static void EEPROM_Read(unsigned char WriteAddr, unsigned char * Data, unsigned char Len)
{
    I2C_SendData(WriteAddr);
    I2C_ReceiveBuf(Data,Len);
}

int main(void)
{
    I2C_TypeInitDef I2C_InitStruct;
    
    //pin function select
    GPIO_FunctionSelect(GPIO_MAP_PIN_A1,GPIO_MAP_PIN_FUNC1); //SCL
    GPIO_FunctionSelect(GPIO_MAP_PIN_A2,GPIO_MAP_PIN_FUNC1); //SDA
    
    I2C_InitStruct.I2C_Mode = I2C_MODE_MASTER;
    I2C_InitStruct.I2C_Addr = 0x50;
    I2C_InitStruct.I2C_Speed = I2C_SPEED_FAST;
    
    I2C_Init(&I2C_InitStruct);
    I2C_Cmd(ENABLE);
    
    EEPROM_Write(0x00, TxBuff, 8);

    my_delay(0x00400000);
    
    EEPROM_Read(0x00, RxBuff, 8);
    
    while(1);
}
