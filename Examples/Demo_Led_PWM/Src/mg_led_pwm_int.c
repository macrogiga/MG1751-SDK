/**
  ******************************************************************************
  * @file    mg_led_pwm_int.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver_pwm_led.h"


void PWM_LED_filldata(u8 FirstFlag);

// for LED PWM, one SHULD enable this irq handler
void BBSeg_IRQHandler(void) //share with the bb seg irq
{
    if(PWM_LED_GetIntStatusValue()) //PWM_LED_FEED_DATA_INT        
    {
        PWM_LED_ClearFeedInt();
        PWM_LED_filldata(0);//continued seg
    }
    else //end
    {          
        PWM_LED_ClearEndInt();
        
        //resend next
        PWM_LED_Unint();
        PWM_LED_filldata(1);//first seg
        PWM_LED_filldata(0);//contiued seg
        
        PWM_LED_Start();        
    }
}
