/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"
#include "mg_driver_lvd.h"


static void led_io_low(void)
{
    GPIO->DATA_OUT_CLEAR = GPIO_PIN_10;
}

static void led_io_hi(void)
{
    GPIO->DATA_OUT_SET = GPIO_PIN_10;
}

static void init_led(void)
{
    GPIO_InitTypeDef cfg;
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A10,GPIO_MAP_PIN_FUNC3);
    
    cfg.GPIO_Mode = GPIO_Mode_Out_PP;
    cfg.GPIO_Pin  = GPIO_PIN_10;
    GPIO_Init(GPIO, &cfg);
    
    GPIO->DATA_OUT_CLEAR = GPIO_PIN_10;
}

int main(void)
{
    init_led(); //test purpose
    
    //if BLE not initialized, one MUST init the SPI as following.
    Init_HW_Spi();
    
    //set the waring level
    mg_SetVddMonitorLevel(0x07);

    //enable monitor
    mg_EnableVddMonitor(1);
    
    //detect if any
    while(1)
    {
        if(mg_getVddWarningFlag())
        {
            led_io_low();
        }
        else
        {
            led_io_hi();
        }
    }
    
    //never be here
}
