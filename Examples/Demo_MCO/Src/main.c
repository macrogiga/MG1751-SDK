/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"
#include "mg_ble_control.h"


int main(void)
{
//    u8 *addr;
    
    
    //===========MCO output sys clock / 4 =========== //
//    CLK_SetDiv(4);//clock div --> sys clock
    Sys_ClockOutEnable(MCO_EN_MCU_CLK_DIV4);
    
    //===========MCO output RC clock =================//
//    Sys_ClockOutEnable(MCO_EN_SYS_RC_CLK);

    //===========MCO output BB 16m clock / 4 ======== //
//    MgBle_Init(0, &addr);
//    Sys_ClockOutEnable(MCO_EN_BB_CLK_16M_DIV4);
    
    while(1);
}
