/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.2
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */

#include "mg_driver_mtp.h"

int main(void)
{
    #define TestAddr (30*1024)
    unsigned char data[8] = {1,2,3,4,5,6,7,8};
    
    ProgramPage(TestAddr,8,data);
    ProgramPage(TestAddr+8,8,data);
    
    EraseSector(TestAddr);
    ProgramPage(TestAddr,8,data);

    while(1);
    
    return 0;
}

