/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"
#include "mg_driver_pwm.h"


#define MAX_COUNT_VALUE (44)

static int my_delay(int num)
{
    int i,j=1;
    
    for(i = 0; i < num ; i ++) j+=i;
    return j;
}

int main(void)
{
    int count = 0;
    
    my_delay(0xa00000*3);//for SWD stop mode safty usage ONLY
    
    //pin function select
    GPIO_FunctionSelect(GPIO_MAP_PIN_A11,GPIO_MAP_PIN_FUNC2); //PWM0
    GPIO_FunctionSelect(GPIO_MAP_PIN_A12,GPIO_MAP_PIN_FUNC2); //PWM1
    GPIO_FunctionSelect(GPIO_MAP_PIN_A13,GPIO_MAP_PIN_FUNC2); //PWM2
    GPIO_FunctionSelect(GPIO_MAP_PIN_A14,GPIO_MAP_PIN_FUNC2); //PWM3
    GPIO_FunctionSelect(GPIO_MAP_PIN_A10,GPIO_MAP_PIN_FUNC2); //PWM4
   
    //init pwm if any
    PWM_Init(PWM_CH0,0,MAX_COUNT_VALUE);
    PWM_Init(PWM_CH1,0,MAX_COUNT_VALUE);
    PWM_Init(PWM_CH2,0,MAX_COUNT_VALUE);
    PWM_Init(PWM_CH3,0,MAX_COUNT_VALUE);
    PWM_Init(PWM_CH4,0,MAX_COUNT_VALUE);
    PWM_SetClockDiv(1);
    PWM_SetEnable(PWM_EN_CH0|PWM_EN_CH1|PWM_EN_CH2|PWM_EN_CH3|PWM_EN_CH4);

    //change the waveform periodically
    while(1)
    {
        my_delay(50000);
        PWM_SetValue(PWM_CH0,count);
        PWM_SetValue(PWM_CH1,count);
        PWM_SetValue(PWM_CH2,count);
        PWM_SetValue(PWM_CH3,count);
        PWM_SetValue(PWM_CH4,count);
//        SysGotoStopMode();
        count ++;
        if(count > MAX_COUNT_VALUE)count = 0;
    }
}
