/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"
#include "mg_driver_rtc.h"
#include "mg_driver_int.h"


static void init_led(void)
{
    GPIO_InitTypeDef cfg;
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A10,GPIO_MAP_PIN_FUNC3);
    
    cfg.GPIO_Mode = GPIO_Mode_Out_PP;
    cfg.GPIO_Pin  = GPIO_PIN_10;
    GPIO_Init(GPIO, &cfg);
    
    GPIO->DATA_OUT_CLEAR = GPIO_PIN_10;
}

int main(void)
{
    u32 temp = 0;
    
    while(temp++ < 0x800000){}; //delay for flash programming before mcu enter low power mode.
    
    init_led();
    
    //init rtc
    Rtc_Init(10);  //1s

    //enable the irq bit, clear pending irq first
    NVIC_IrqClearPendingIRQ(RTC_IRQn);
    NVIC_EnableIRQ(INT_MASK_BIT_RTC);
    
    Rtc_StartRtc();
    
    while(1)
    {
        GPIO_ToggleBits(GPIO, GPIO_PIN_10); //led toggle once when RTC trigger

        SysGotoStopMode();
        //SysGotoStandbyMode();
    }
}
