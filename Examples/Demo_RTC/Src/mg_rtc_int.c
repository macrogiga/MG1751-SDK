/**
  ******************************************************************************
  * @file    mg_rtc_int.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver_rtc.h"



void RTC_IRQHandler(void)
{
    Rtc_IntClear();
}
