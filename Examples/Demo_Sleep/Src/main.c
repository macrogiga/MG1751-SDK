/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"


/*
in SLEEP mode, chip can be woke up by Timer, RTC, external int...
*/

static void init_led(void)
{
    GPIO_InitTypeDef cfg;
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A10,GPIO_MAP_PIN_FUNC3);
    
    cfg.GPIO_Mode = GPIO_Mode_Out_PP;
    cfg.GPIO_Pin  = GPIO_PIN_10;
    GPIO_Init(GPIO, &cfg);

    GPIO_ResetBits(GPIO, GPIO_PIN_10); //L = led_on
}

int main(void)
{
    init_led();

    //enable the irq bit
    NVIC_EnableIRQ(INT_MASK_BIT_TIMER);
    
    Timer2_Init(39000*1000, TIMER_PERIODIC_RUNNING_MODE);  //irq enabled, clock div = 1;
    
    while(1)
    {
        SysGotoSleepMode();
    }
}
