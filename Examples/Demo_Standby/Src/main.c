/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"


/*
in STANDBY mode, chip can be woke up by PA0(rising edge), RTC
*/

//IO_KEY = GPIO_PIN_0
//IO_LED = GPIO_PIN_10

static void init_led(void)
{
    GPIO_InitTypeDef cfg;
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A10,GPIO_MAP_PIN_FUNC3);
    
    cfg.GPIO_Mode = GPIO_Mode_Out_PP;
    cfg.GPIO_Pin  = GPIO_PIN_10;
    GPIO_Init(GPIO, &cfg);

    GPIO_ResetBits(GPIO, GPIO_PIN_10); //L = led_on
}

static int my_delay(int num)
{
    int i,j=1;
    
    for(i = 0; i < num ; i ++) j+=i;
    return j;
}

int main(void)
{
    my_delay(0x00800000); //delay for flash programming.

    init_led();
    my_delay(0x80000);
    GPIO_SetBits(GPIO, GPIO_PIN_10); //L = led_off
    
    SysGotoStandbyMode(); //press key to wake up
    
    while(1)
    {
        GPIO_ResetBits(GPIO, GPIO_PIN_10); //L = led_on
    }
}
