/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"

/*
in STOP mode, chip can be woke up by extINT, RTC, ...
*/

//IO_KEY = GPIO_PIN_0
//IO_LED = GPIO_PIN_10
static void init_key(void)
{
    GPIO_InitTypeDef cfg;
    GPIO_EXTInt_TypeDef type;
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A0,GPIO_MAP_PIN_FUNC1);
    
    cfg.GPIO_Mode = GPIO_Mode_IPD;
    cfg.GPIO_Pin  = GPIO_PIN_0;
    GPIO_Init(GPIO, &cfg);
    
    //external irq cfg
    type.GPIO_Pin = GPIO_PIN_0;
    type.Trigger_Type = GPIO_Triger_RISING_EDGE; //GPIO_Triger_HIGH 
    GPIO_EXTInt_Init(GPIO,&type);
    
    //enable gpio irq
    NVIC_EnableIRQ(INT_MASK_BIT_GPIO);
}

static void init_led(void)
{
    GPIO_InitTypeDef cfg;
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A10,GPIO_MAP_PIN_FUNC3);
    
    cfg.GPIO_Mode = GPIO_Mode_Out_PP;
    cfg.GPIO_Pin  = GPIO_PIN_10;
    GPIO_Init(GPIO, &cfg);

    GPIO_ResetBits(GPIO, GPIO_PIN_10); //L = led_on
}

static int my_delay(int num)
{
    int i,j=1;
    
    for(i = 0; i < num ; i ++) j+=i;
    return j;
}

int main(void)
{
    my_delay(0x00800000); //delay for flash programming.

    init_led();
    init_key();
    
    while(1)
    {
        my_delay(0x10000);
        GPIO_ToggleBits(GPIO, GPIO_PIN_10);
        
        SysGotoStopMode();
        
        GPIO_ResetBits(GPIO, GPIO_PIN_10); //led_on
    }
}

void GPIO_IRQHandler(void)
{
    if(GPIO_PIN_0 & GPIO->INT_STATUS)
    {
        GPIO->INT_STATUS = GPIO_PIN_0; //clear irq
        
        GPIO_ToggleBits(GPIO, GPIO_PIN_10);
    }
}
