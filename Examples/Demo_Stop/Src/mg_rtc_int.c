/**
  ******************************************************************************
  * @file    mg_rtc_int.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver_rtc.h"


u8 flag = 0;

void toggle_io(void)
{
    if(flag) GPIO->DATA_OUT_SET = GPIO_PIN_0;
    else GPIO->DATA_OUT_CLEAR = GPIO_PIN_0;
    
    flag = !flag;
}


void RTC_IRQHandler(void)
{
    Rtc_IntClear();
    //toggle io here if any, to do...
    toggle_io();
}
