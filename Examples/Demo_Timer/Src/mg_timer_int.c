/**
  ******************************************************************************
  * @file    mg_timer_int.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"


void TIMER_IRQHandler(void)
{
    if(Timer1_int_status())
    {
        Timer1_int_clear();
        
        GPIO_ToggleBits(GPIO, GPIO_PIN_10);
    }
    
    if(Timer2_int_status())
    {
        Timer2_int_clear();
        
        GPIO_ToggleBits(GPIO, GPIO_PIN_10);
    }
}
