/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include <stdio.h>
#include "mg_driver.h"
#include "mg_driver_uart.h"


int main(void)
{
    //enable the irq bit
    NVIC_EnableIRQ(INT_MASK_BIT_UART);
    
    //init uart
    UART_Set_baudrate(19200);
//    UART_Enable_FlowControl();
    
    printf("\r\nMG1751 Uart Demo!");

    while(1)
    {
        //auto loop, pleas see the UART_IRQHandler()
        ;
    }
}
