/**
  ******************************************************************************
  * @file    mg_uart_int.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include <stdio.h>
#include "mg_driver_uart.h"


int fputc(int ch, FILE *f)
{
    UART_SendOneByte((u8) ch);
    while(!UART_Get_TX_Flag()); //wait for empty
    return ch;
}

void UART_IRQHandler(void)
{    
    while(UART_Get_RX_Flag())
    {
        UART_SendOneByte(UART_ReceiveByte()); //loop
        while(!UART_Get_TX_Flag()); //wait for empty
    }
}
