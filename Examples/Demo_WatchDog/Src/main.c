/**
  ******************************************************************************
  * @file    main.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver.h"
#include "mg_driver_watchdog.h"


static void init_led(void)
{
    GPIO_InitTypeDef cfg;
    
    GPIO_FunctionSelect(GPIO_MAP_PIN_A10,GPIO_MAP_PIN_FUNC3);
    
    cfg.GPIO_Mode = GPIO_Mode_Out_PP;
    cfg.GPIO_Pin  = GPIO_PIN_10;
    GPIO_Init(GPIO, &cfg);

    GPIO_ResetBits(GPIO, GPIO_PIN_10); //L = led_on
}

static int mydelay(int num)
{
    int i,j=0;
    
    for(i = 0 ; i < num ; i ++)
    {
        j += i;
    }
    
    return j;
}

int main(void)
{
    mydelay(0xA00000);
    
    init_led();
    
    // !!!IMPORTANT: One SHOULD enable the irq bit!!!
    NVIC_EnableIRQ(INT_MASK_BIT_WATCHDOG);
    
    WDog_Init(5*32768);  //irq enabled, clock div = 1; clk:32768Hz
    
    while(1)
    {
//        WDog_Feed(10*32768);  //comment this line for WDG reset mcu
        mydelay(0xA0000);
        GPIO_ToggleBits(GPIO, GPIO_PIN_10); //led flash
    }
}
