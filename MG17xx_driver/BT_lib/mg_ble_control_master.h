/**
  ******************************************************************************
  * @file    mg_ble_control_master.h
  * @author  
  * @version V0.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#ifndef __H_MG_BLE_CONTROL_MASTER_H__
#define __H_MG_BLE_CONTROL_MASTER_H__

#define SCAN_TYPE_ACTIVE   0x01
#define SCAN_TYPE_PASSIVE  0x00


//#define PERIPH_CONNECT_DISCONNECTED     0x00
//#define PERIPH_CONNECT_CONNECTED        0x01
#define CENTRAL_CONNECT_CONNECTED       0x02  /*connection success*/
#define CENTRAL_CONNECT_ATTEMPT_FAILED  0x03  /*try connection failed after NN attempts*/
#define CENTRAL_CONNECT_DISCONNECTED    0x04  /*connection lost*/


typedef struct _tagBleDevInfo{
    u8 addr[6];
    u8 adv_type;  /*ADV_IND,ADV_DIRECT_IND,ADV_NONCONN_IND,ADV_SCAN_RSP*/
    u8 addr_type; /*ADDR_TYPE_XXXX*/
    u8 adv[31];
    u8 rssi;
    u8 rsp[31];
    u8 reserved;
}BLE_DEV_INFO;

typedef struct _tagScanCfg{
    u16 scan_interval;//unit 0.625ms
    u16 scan_window;  //unit 0.625ms
    u8  scan_type;
    u8  txAddrType;  //scan req addr(local) type 0(public) or 1(random)
}MASTER_SCAN_CFG;

typedef struct _tagMasterConnectCfg{
    u8  targetAddr[6];    
    u8  targetAddrType; //target addr type 0(public) or 1(random)
    u8  txAddrType;     //local  addr type 0(public) or 1(random)
        
    u16 connect_interval;//unit 1.25ms
    u8  attempt_num;
}MASTER_CONNECT_CFG;


u8   mconn_writedata(u16 writehandle,u8* data, u8 len);//returns bytes send
void mconn_findByTypeValueReq(u16 uuid16); //send find primary service of uuid16
void mconn_readByGrpReq(u16 handle_start,u16 handle_end, u16 uuid16); //primary service uuid16:0x2800
void mconn_readByTypeReq(u16 handle_start,u16 handle_end, u16 uuid16); //read character uuid16:0x2803
void mconn_readReq(u16 handle); //read req
void mconn_findInfoReq(u16 handle_start, u16 handle_end); //find info req

typedef void (*callback)(u8 evt, u8* contxt);

void MgBle_CentralScan_Init(MASTER_SCAN_CFG* cfg);
u8   MgBle_CentralScan_Run(callback); //when irq then running this one step
void MgBle_CentralScan_Stop(void);
u8   MgBle_GetCentralScanRunContinueFlag(void);

void MgBle_CentralConnect_Init(MASTER_CONNECT_CFG* cfg);
u8   MgBle_CentralConnect_Run(callback);
void MgBle_Central_Disconnect(void);

void MgBle_ConnStatusUpdate_Central(u8 Status /*CENTRAL_CONNECT_XXXXX*/); //central connect status callback, porting function
void CALLBACK_Master_AttError(u8 ReqOpCode,u8 *par/*handle16,reason8*/); //porting API

//25 <= len <= LongPktMaxTxOctets - 7
u8 mconn_writeLongPktData(u16 handle,u8* data, u8 len);//return 1 means OK

u8 gatt_writedata(u16 writehandle,u8* data, u8 len);//returns bytes send,long or short package supported.
#endif
