;******************** (C) COPYRIGHT 2020 Macrogiga Electronics******************
;* File Name          : startup_mg.s
;* Author             : 
;* Version            : V1.0.0
;* Date               : 2020
; 
;*******************************************************************************
;

Stack_Size      EQU     0x00000400

                AREA    STACK, NOINIT, READWRITE, ALIGN=3
Stack_Mem       SPACE   Stack_Size
__initial_sp


; <h> Heap Configuration
;   <o>  Heap Size (in Bytes) <0x0-0xxxX00:8>
; </h>

Heap_Size       EQU     0x00000400

                AREA    HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem        SPACE   Heap_Size
__heap_limit

                PRESERVE8
                THUMB


; Vector Table Mapped to Address 0 at Reset
                AREA    RESET, DATA, READONLY
                EXPORT  __Vectors
                EXPORT  __Vectors_End
                EXPORT  __Vectors_Size

__Vectors       DCD     __initial_sp                   ; Top of Stack
                DCD     Reset_Handler                  ; Reset Handler
                DCD     NMI_Handler                    ; NMI Handler
                DCD     HardFault_Handler              ; Hard Fault Handler
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     SVC_Handler                    ; SVCall Handler
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     PendSV_Handler                 ; PendSV Handler
                DCD     SysTick_Handler                ; SysTick Handler

                ; External Interrupts
                DCD     BBSeg_IRQHandler               ; Baseband Seg irq handler / led pwm feed data irq
                DCD     UART_IRQHandler                ; UART irq handler
                DCD     BB_IRQHandler                  ; BB-wakeup&sleep irq handler
                DCD     RTC_IRQHandler                 ; RTC irq handler
                DCD     WDG_IRQHandler                 ; watch dog irq handler
                DCD     LOW_VOLTAGE_IRQHandler         ; low voltage power irq handler 
                DCD     GPIO_IRQHandler                ; GPIO irq handler
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     TIMER_IRQHandler               ; timer irq handler
                DCD     0                              ; Reserved
                DCD     I2C_IRQHandler                 ; I2C irq handler
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                
__Vectors_End

__Vectors_Size  EQU  __Vectors_End - __Vectors

                AREA    |.text|, CODE, READONLY

; Reset handler routine
Reset_Handler    PROC
                 EXPORT  Reset_Handler                 [WEAK]

        IMPORT  __main
        IMPORT  SystemInit


        LDR     R0, =__initial_sp          ; set stack pointer 
        MSR     MSP, R0  

ApplicationStart
                 LDR     R0, =SystemInit
                 BLX     R0
                 LDR     R0, =__main
                 BX      R0
                 ENDP

; Dummy Exception Handlers (infinite loops which can be modified)

NMI_Handler     PROC
                EXPORT  NMI_Handler                    [WEAK]
                B       .
                ENDP
HardFault_Handler\
                PROC
                EXPORT  HardFault_Handler              [WEAK]
                B       .
                ENDP
SVC_Handler     PROC
                EXPORT  SVC_Handler                    [WEAK]
                B       .
                ENDP
PendSV_Handler  PROC
                EXPORT  PendSV_Handler                 [WEAK]
                B       .
                ENDP
SysTick_Handler PROC
                EXPORT  SysTick_Handler                [WEAK]
                B       .
                ENDP

Default_Handler PROC

                EXPORT  BBSeg_IRQHandler               [WEAK]
                EXPORT  UART_IRQHandler                [WEAK]
                EXPORT  BB_IRQHandler                  [WEAK]
                EXPORT  RTC_IRQHandler                 [WEAK]
                EXPORT  WDG_IRQHandler                 [WEAK]
                EXPORT  LOW_VOLTAGE_IRQHandler         [WEAK]
                EXPORT  GPIO_IRQHandler                [WEAK]
                EXPORT  TIMER_IRQHandler               [WEAK]
                EXPORT  I2C_IRQHandler                 [WEAK]

BBSeg_IRQHandler
UART_IRQHandler
BB_IRQHandler
RTC_IRQHandler
WDG_IRQHandler
GPIO_IRQHandler
LOW_VOLTAGE_IRQHandler
TIMER_IRQHandler
I2C_IRQHandler

                B       .

                ENDP

                ALIGN

;*******************************************************************************
; User Stack and Heap initialization
;*******************************************************************************
                 IF      :DEF:__MICROLIB
                
                 EXPORT  __initial_sp
                 EXPORT  __heap_base
                 EXPORT  __heap_limit
                
                 ELSE
                
                 IMPORT  __use_two_region_memory
                 EXPORT  __user_initial_stackheap
                 
__user_initial_stackheap

                 LDR     R0, =  Heap_Mem
                 LDR     R1, =(Stack_Mem + Stack_Size)
                 LDR     R2, = (Heap_Mem +  Heap_Size)
                 LDR     R3, = Stack_Mem
                 BX      LR

                 ALIGN

                 ENDIF

                 END

;************************ (C) COPYRIGHT Macrogiga Electornics *****END OF FILE*****
