/******************** (C) COPYRIGHT 2019 Macrogiga Electronics******************
 * File Name          : system.c
 * Author             : 
 * Version            : V1.0.0
 * Date               : 2020  
 ******************************************************************************/
#include "mg_driver_sys.h"
#include "mg_driver_mtp.h"

/**
* @brief  Setup the microcontroller system
*         Initialize the Embedded Flash Interface, the PLL and update the 
*         SystemCoreClock variable.
* @note   This function should be used only after reset.
* @param  None
* @retval None
*/
void SystemInit (void)
{
    SYS_MG_ROM_CFG->CCR = 0x08;
    SYS_MG_ROM_CFG->CCR = 0x0C;
    while(!(SYS_MG_ROM_CFG->SR & 0x10));
    SYS_MG_ROM_CFG->CCR = 0x0D;
    MG_SYS_ROM_CTRL->Reg = 1;
    MTP_CTRL->TPGHF = 0x30C;
    MTP_CTRL->TCERH = 0x594390;
    
    SYS_PMU_REG->Pmu_enable = 1; //enabled the Pmu, including disable cpu 32k clock @ stop mode(delay 5 clock)
}

/******************************************************************************/
/*            Cortex-M0 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
    int i=1;
    
    while(1)i++;
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

//static unsigned int SysTick_Count = 0;

//unsigned int GetSysTickCount(void) //porting api
//{
//    return SysTick_Count;
//}


///**
//  * @brief  This function handles SysTick Handler.
//  * @param  None
//  * @retval None
//  */
//void SysTick_Handler(void)
//{
//    SysTick_Count ++;
//}
