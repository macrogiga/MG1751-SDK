/**
  ******************************************************************************
  * @file    mg_driver.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
  
#ifndef __MG_DRIVER_H__
#define __MG_DRIVER_H__

#include "mg_driver_types.h"

#include "mg_driver_ble.h"
#include "mg_driver_spi_master.h"

#include "mg_driver_power.h"
#include "mg_driver_clock.h"
#include "mg_driver_int.h"
#include "mg_driver_sys.h"
#include "mg_driver_gpio.h"
#include "mg_driver_timer.h"
#include "mg_driver_uart.h"
#include "mg_driver_i2c.h"
#include "mg_driver_watchdog.h"

#endif
