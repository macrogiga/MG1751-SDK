/**
  ******************************************************************************
  * @file    mg_driver_aes.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
  
#ifndef _MG_DRIVER_AES_H_  
#define _MG_DRIVER_AES_H_

#include "mg_driver_header.h"

#define AES_SWAP_NONE        0x00
#define AES_START            0x01
#define AES_DOUT_SWAP        0x01
#define AES_DIN_SWAP         0x02

typedef struct _tag_MG_AES
{
    __IO  u32 START;//aes_trig(start), write only
    __IO  u32 KEY0;
    __IO  u32 KEY1;
    __IO  u32 KEY2;
    __IO  u32 KEY3;
    __IO  u32 DIN0;
    __IO  u32 DIN1;
    __IO  u32 DIN2;
    __IO  u32 DIN3;
    __IO  u32 DOUT0;
    __IO  u32 DOUT1;
    __IO  u32 DOUT2;
    __IO  u32 DOUT3;
    __IO  u32 ENC_DONE; //read only
    __IO  u32 DATA_SWAP; //byt_swap_input,byte_swap_output
}_AES_;

#define AES              ((_AES_ *) AES_BASE)

void AES_Init(void);
void AES_UnInit(void);
void AES_Enc(u8 *In, u8 *Key, u8 *Out, u8 NewKeyFlag);

#endif

