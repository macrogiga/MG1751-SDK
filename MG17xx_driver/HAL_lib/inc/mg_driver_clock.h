/**
  ******************************************************************************
  * @file    mg_driver_clock.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
  
#ifndef _MG_DRIVER_CLOCK_H_
#define _MG_DRIVER_CLOCK_H_

#include "mg_driver_header.h"

typedef struct _tag_MG_CLK
{
    __IO  u32 CLK_CFG_CTRL;
    __IO  u32 CLK_GATE;
}_MG_CLK_;

#define SYS_CLK              ((_MG_CLK_ *) (SYS_CTRL_BASE+0x08))

#define CLK_SEL_SYS                     0x00000000
#define CLK_SEL_SYS_DIV_ENABLE          0x00000001
#define CLK_SEL_LOW                     0x00000002
#define CLK_SEL_ENABLE                  0x00000004

//////////////////////clock gating definition//////////////////////////////
#define CLK_GATE_GPIO_EN                     0x00000008
#define CLK_GATE_PWM_EN                      0x00000010
#define CLK_GATE_UART_EN                     0x00000020
#define CLK_GATE_I2C_EN                      0x00000040
#define CLK_GATE_I2CMP_EN                    0x00000080
#define CLK_GATE_SPIM_EN                     0x00000100
#define CLK_GATE_AES_EN                      0x00000200
#define CLK_GATE_TIMER_EN                    0x00000400
#define CLK_GATE_WATCHDOG_EN                 0x00000800
#define CLK_GATE_RTC_EN                      0x00001000
#define CLK_GATE_LED_PWM_EN                  0x00002000
//////////////////////////////////////////////////////////////////////////

void CLK_SetDiv(u32 Div); //0~16, 0 means low clock selected, others means high clock selected.

u32  CLK_GetClockGateBits(void); //returns clock gating mask-bits result.
void CLK_SetClockGateBits(u32 clk_gate_bits);//seting all clock gating bits

void CLK_SetClockGate(u32 clk_gate_bit); //enable one module's clock
void CLK_ClearClockGate(u32 clk_gate_bit);//disable one module's clock

#endif

