/**
  ******************************************************************************
  * @file    mg_driver_gpio.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
  
#ifndef _MG_DRIVER_GPIO_H_
#define _MG_DRIVER_GPIO_H_

#include "mg_driver_header.h"

#define GPIO_MAP_PIN_A0   0
#define GPIO_MAP_PIN_A1   1
#define GPIO_MAP_PIN_A2   2
#define GPIO_MAP_PIN_A3   3
#define GPIO_MAP_PIN_A4   4
#define GPIO_MAP_PIN_A5   5
#define GPIO_MAP_PIN_A6   6
#define GPIO_MAP_PIN_A7   7
#define GPIO_MAP_PIN_A8   8
#define GPIO_MAP_PIN_A9   9
#define GPIO_MAP_PIN_A10 10
#define GPIO_MAP_PIN_A11 11
#define GPIO_MAP_PIN_A12 12
#define GPIO_MAP_PIN_A13 13
#define GPIO_MAP_PIN_A14 14
#define GPIO_MAP_PIN_A15 15

#define GPIO_MAP_PIN_FUNC1   0x00000000 /* primary function, default */
#define GPIO_MAP_PIN_FUNC2   0x00000001 /* second function */
#define GPIO_MAP_PIN_FUNC3   0x00000002 /* third function */
#define GPIO_MAP_PIN_FUNC4   0x00000003 /* MP-scan */


/** 
  * @brief  GPIO Bit SET and Bit RESET enumeration 
  */ 
typedef enum
{ 
  Bit_RESET = 0,
  Bit_SET
}BitAction;
#define IS_GPIO_BIT_ACTION(ACTION) (((ACTION) == Bit_RESET) ||\
                                    ((ACTION) == Bit_SET))

void GPIO_FunctionSelect(u32 GPIO_MAP_PINx, u32 GPIO_MAP_PIN_FUNCx);
void GPIO_Init(GPIO_TypeDef* GPIOx, GPIO_InitTypeDef* GPIO_InitStruct);

u8 GPIO_ReadInputDataBit(GPIO_TypeDef* GPIOx, u16 GPIO_Pin);
u16 GPIO_ReadInputData(GPIO_TypeDef* GPIOx);
u8 GPIO_ReadOutputDataBit(GPIO_TypeDef* GPIOx, u16 GPIO_Pin);
u16 GPIO_ReadOutputData(GPIO_TypeDef* GPIOx);
void GPIO_SetBits(GPIO_TypeDef* GPIOx, u16 GPIO_Pin);
void GPIO_ResetBits(GPIO_TypeDef* GPIOx, u16 GPIO_Pin);
void GPIO_ToggleBits(GPIO_TypeDef* GPIOx, u16 GPIO_Pin);

void GPIO_EXTInt_Init(GPIO_TypeDef* GPIOx, GPIO_EXTInt_TypeDef* type);





#endif
