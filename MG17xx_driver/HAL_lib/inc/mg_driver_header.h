/**
  ******************************************************************************
  * @file    mg_driver_header.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */

#ifndef _MG_DRIVER_HEADRT_H_
#define _MG_DRIVER_HEADRT_H_

#include "mg_driver_types.h"


typedef struct _tag_SysCtrl
{
    __IO  u32 FUNCTION_SEL;
    __IO  u32 RESET_CTRL;
    __IO  u32 CLK_CFG_CTRL;
    __IO  u32 CLK_GATE;
    __IO  u32 rsv;
    __IO  u32 PWR_CTRL;
    __IO  u32 MTP_CTRL;
    __IO  u32 rsv2;
    __IO  u32 STANDALONE_REG0;
    __IO  u32 STANDALONE_REG1;
    __IO  u32 CORE_VERSION;
    __IO  u32 PMU_ENABLE;
    __IO  u32 WKUP_SCAN_CTRL;
    __IO  u32 WAKEUP_DELAY_CNT;
    __IO  u32 PIN_SELECT;
    __IO  u32 SPI_IN_CS_N;
    __IO  u32 PA_CONF[16];
}SysCtrl_Type;

#define SysCtrl              ((SysCtrl_Type *) (SYS_CTRL_BASE+0x00))

////////////////////SYSTEM ADDR MAP INFO////////////////////
#define GPIO_PIN_0           ((u16)0x0001)  /* Pin 0 selected */
#define GPIO_PIN_1           ((u16)0x0002)  /* Pin 1 selected */
#define GPIO_PIN_2           ((u16)0x0004)  /* Pin 2 selected */
#define GPIO_PIN_3           ((u16)0x0008)  /* Pin 3 selected */
#define GPIO_PIN_4           ((u16)0x0010)  /* Pin 4 selected */
#define GPIO_PIN_5           ((u16)0x0020)  /* Pin 5 selected */
#define GPIO_PIN_6           ((u16)0x0040)  /* Pin 6 selected */
#define GPIO_PIN_7           ((u16)0x0080)  /* Pin 7 selected */
#define GPIO_PIN_8           ((u16)0x0100)  /* Pin 8 selected */
#define GPIO_PIN_9           ((u16)0x0200)  /* Pin 9 selected */
#define GPIO_PIN_10          ((u16)0x0400)  /* Pin 10 selected */
#define GPIO_PIN_11          ((u16)0x0800)  /* Pin 11 selected */
#define GPIO_PIN_12          ((u16)0x1000)  /* Pin 12 selected */
#define GPIO_PIN_13          ((u16)0x2000)  /* Pin 13 selected */
#define GPIO_PIN_14          ((u16)0x4000)  /* Pin 14 selected */
#define GPIO_PIN_15          ((u16)0x8000)  /* Pin 15 selected */
#define GPIO_Pin_All         ((u16)0xFFFF)  /* All pins selected */

#define GPIO_OUTPUT         0x10
#define GPIO_INPUT          0x00

typedef struct
{
  __IO u32 DATA_IN;
  __IO u32 DATA_OUT;
  __IO u32 DATA_OUT_SET;
  __IO u32 DATA_OUT_CLEAR;
  __IO u32 OUT_EN_SET;
  __IO u32 OUT_EN_CLR;
  __IO u32 ALT_FUNC_SET; //not used
  __IO u32 ALT_FUNC_CLR; //not used
  __IO u32 INT_EN_SET;
  __IO u32 INT_EN_CLR;
  __IO u32 INT_TYPE_SET;   // irq type {type,pol}: {0,0 low level}, {1,0 falling edge}, {0,1 high level}, {1,1 rising edge}
  __IO u32 INT_TYPE_CLR;
  __IO u32 INT_POL_SET;
  __IO u32 INT_POL_CLR;
  __IO u32 INT_STATUS;  //write to clear interrupt flag
} GPIO_TypeDef;

typedef enum
{
    GPIO_Mode_IN_FLOATING = 0x00, //浮空输入
    GPIO_Mode_IPD = 0x01,      //下拉输入
    GPIO_Mode_IPU = 0x02,      //上拉输入
    GPIO_Mode_Out_OD = 0x10,   //通用开漏输出
    GPIO_Mode_Out_PP = 0x14    //通用推免输出
}GPIOMode_TypeDef;


typedef struct _tag_GPIO_InitTypeDef
{
  u16 GPIO_Pin;
  u16 rsv;
  GPIOMode_TypeDef GPIO_Mode;
}GPIO_InitTypeDef;


typedef enum
{ 
    GPIO_Triger_LOW = 0,
    GPIO_Triger_FALLING_EDGE = 1,
    GPIO_Triger_HIGH = 2,
    GPIO_Triger_RISING_EDGE = 3
}GPIO_Trigger_TypeDef;

typedef struct _tag_GPIO_EXTInt_TypeDef
{
    u16 GPIO_Pin;
    u16 rsv;
    GPIO_Trigger_TypeDef Trigger_Type;
    
}GPIO_EXTInt_TypeDef;

#define AES_BASE            ((u32)0x40000000)
#define PWM_LED_BASE        ((u32)0x40001000)
#define TIMER_BASE          ((u32)0x40002000)
#define UART_BASE           ((u32)0x40004000)
#define PWM_BASE            ((u32)0x40005000)
#define RTC_BASE            ((u32)0x40006000)
#define SYS_CTRL_ROM_BASE   ((u32)0x40007000)
#define WDOG_BASE           ((u32)0x40008000)
#define SPIM_BASE           ((u32)0x4000C000)
#define I2C_BASE            ((u32)0x4000D000)
#define GPIO_BASE           ((u32)0x40010000)
#define SYS_CTRL_BASE       ((u32)0x4001F000)

#define GPIO                ((GPIO_TypeDef *) GPIO_BASE)

typedef struct _tag_WDOG_TypeDef
{
  __IO  u32 WDOGLOAD;
  __IO  u32 WDOGVALUE;
  __IO  u32 WDOGCONTROL;
  __IO  u32 WDOGINTCLR;
}WDOG_TypeDef;

#define WDOG               ((WDOG_TypeDef *) WDOG_BASE)

typedef struct _tag_TIMER_TypeDef
{
  __IO  u32 TIMER1LOAD;
  __IO  u32 TIMER1VALUE;
  __IO  u32 TIMER1CONTROL;
  __IO  u32 TIMER1INTCLR;
  __IO  u32 TIMER1RIS;
  __IO  u32 TIMER1MIS;
  __IO  u32 TIMER1BGLOAD;
    
  __IO  u32 Rsv;
    
  __IO  u32 TIMER2LOAD;
  __IO  u32 TIMER2VALUE;
  __IO  u32 TIMER2CONTROL;
  __IO  u32 TIMER2INTCLR;
  __IO  u32 TIMER2RIS;
  __IO  u32 TIMER2MIS;
  __IO  u32 TIMER2BGLOAD;
}TIMER_TypeDef;

#define TIMER_ADDR               ((TIMER_TypeDef *) TIMER_BASE)

typedef struct _tag_UART_TypeDef
{
  __IO  u32 RBR_THR_DLL; //8 bit-r/w,  1b1-DLL //div - low
  __IO  u32 DLH_IER; //div - hight,Interrupt Enable Register
  __IO  u32 IIR_FCR; //Interrupt Identification Register,FIFO Control Register
  __IO  u32 LCR;     //Line Control Register
  __IO  u32 MCR;     //Modem Control Register
  __IO  u32 LSR;     //Line Status Register
}UART_TypeDef;

#define UART_ADDR               ((UART_TypeDef *) UART_BASE)

#endif

