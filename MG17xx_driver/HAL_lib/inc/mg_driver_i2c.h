/**
  ******************************************************************************
  * @file    mg_driver_i2c.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
  
#ifndef _MG_DRIVER_I2C_MASTER_H_
#define _MG_DRIVER_I2C_MASTER_H_

#include "mg_driver_header.h"

typedef struct _tag_I2CM_TypeDef
{
  __IO  u32 I2C_CON;            //00
  __IO  u32 I2C_TAR;            //04
  __IO  u32 I2C_SAR;            //08
  __IO  u32 I2C_HS_MADDR;       //0c
  __IO  u32 I2C_DATA_CMD;       //10
  __IO  u32 I2C_SS_SCL_HCNT;    //14
  __IO  u32 I2C_SS_SCL_LCNT;    //18
  __IO  u32 I2C_FS_SCL_HCNT;    //1c
  __IO  u32 I2C_FS_SCL_LCNT;    //20
  __IO  u32 I2C_HS_SCL_HCNT;    //24
  __IO  u32 I2C_HS_SCL_LCNT;    //28
  __IO  u32 I2C_INTR_STAT;      //2c
  __IO  u32 I2C_INTR_MASK;      //30
  __IO  u32 I2C_RAW_INTR_STAT;  //34
  __IO  u32 I2C_RX_TL;          //38
  __IO  u32 I2C_TX_TL;          //3c
  __IO  u32 I2C_CLR_INTR;       //40
  __IO  u32 I2C_CLR_RX_UNDER;   //44
  __IO  u32 I2C_CLR_RX_OVER;    //48
  __IO  u32 I2C_CLR_TX_OVER;    //4c
  __IO  u32 I2C_CLR_RD_REQ;     //50
  __IO  u32 I2C_CLR_TX_ABRT;    //54
  __IO  u32 I2C_CLR_RX_DONE;    //58
  __IO  u32 I2C_CLR_ACTIVITY;   //5c
  __IO  u32 I2C_CLR_STOP_DET;   //60
  __IO  u32 I2C_CLR_START_DET;  //64
  __IO  u32 I2C_CLR_GEN_CALL;   //68
  __IO  u32 I2C_ENABLE;         //6c
  __IO  u32 I2C_STATUS;         //70
  __IO  u32 I2C_TXFLR;          //74
  __IO  u32 I2C_RXFLR;          //78
  __IO  u32 I2C_SDA_HOLD;       //7c
  __IO  u32 I2C_TX_ABRT_SOURCE; //80
  __IO  u32 I2C_SLV_DATA_NACK_ONLY;//84
  __IO  u32 I2C_DMA_CR;         //88
  __IO  u32 I2C_DMA_TDLR;       //8c
  __IO  u32 I2C_DMA_RDLR;       //90
  __IO  u32 I2C_SDA_SETUP;      //94
  __IO  u32 I2C_ACK_GENERAL_CALL;//98
  __IO  u32 I2C_ENABLE_STATUS;   //9c
    //...
}I2C_TypeDef;



typedef struct _tag_I2CM_TypeInitDef
{
    u16 I2C_Mode;
    u8  I2C_Speed;
    u8  I2C_Addr;    // 7 bit addr
}I2C_TypeInitDef;

#define I2C                     ((I2C_TypeDef *) I2C_BASE)

#define I2C_SLAVE_DISABLE       (0x0001<<6)
#define I2C_SLAVE_ENABLE        (0x0000<<6)

#define I2C_RESTART_EN          (0x0001<<5)
#define I2C_RESTART_DI          (0x0000<<5)

#define I2C_SPEED_FAST          (0x0001<<2)   /*2: fast mode <= 400 kbit/s)*/
#define I2C_SPEED_STANDARD      (0x0001<<1)   /*1: standard mode (0 to 100 kbit/s)*/

#define I2C_MODE_MASTER         (I2C_SLAVE_DISABLE|0x0001)
#define I2C_MODE_SLAVE          0x0000

#define I2C_READ                (0x0001<<8)
#define I2C_WRITE               (0x0000<<8)
#define I2C_STOP                (0x0001<<9)

#define I2C_RX_UNDER            0x0001
#define I2C_RX_OVER             (0x0001<<1)
#define I2C_RX_FULL             (0x0001<<2)
#define I2C_TX_OVER             (0x0001<<3)
#define I2C_TX_EMPTY            (0x0001<<4)
#define I2C_RD_REQ              (0x0001<<5)
#define I2C_TX_ABRT             (0x0001<<6)
#define I2C_RX_DONE             (0x0001<<7)
#define I2C_ACTIVITY            (0x0001<<8)
#define I2C_STOP_DET            (0x0001<<9)
#define I2C_START_DET           (0x0001<<10)
#define I2C_GEN_CALL            (0x0001<<11)

#define I2C_STATUS_ACTIVITY     0x0001
#define I2C_STATUS_TFNF         (0x0001<<1)
#define I2C_STATUS_TFE          (0x0001<<2)
#define I2C_STATUS_RFNE         (0x0001<<3)
#define I2C_STATUS_RFF          (0x0001<<4)
#define I2C_STATUS_M_ACTIVITY   (0x0001<<5)
#define I2C_STATUS_S_ACTIVITY   (0x0001<<6)

void I2C_Init(I2C_TypeInitDef* I2C_InitStruct);
void I2C_Cmd(FunctionalState NewState);
void I2C_GenerateSTOP(void);
void I2C_GeneralCallCmd(FunctionalState NewState);
void I2C_ITConfig(u16 I2C_IT,FunctionalState NewState);
u16 I2C_GetEvent(void);
ITStatus I2C_GetITStatus(u16 I2C_IT);
void I2C_ClearIT(u16 I2C_IT);

void I2C_SendData(u8 const Data);
void I2C_SendDataAndStop(u8 const Data);
u8 I2C_ReceiveData(void);
u8 I2C_ReceiveDataAndStop(void);
void I2C_SendBuf(u8 const *Buf,u8 const Len);
void I2C_ReceiveBuf(u8 *Buf,u8 const Len);
void I2C_GenerateSTOP(void);


void IICS_Write(u32 Address,u32 Data);

#endif
