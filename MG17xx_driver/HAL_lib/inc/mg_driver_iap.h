/**
  ******************************************************************************
  * @file    mg_driver_iap.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */

#ifndef _MG_DRIVER_IAP_H_
#define _MG_DRIVER_IAP_H_

#include "mg_driver_header.h"

#define OTA_OK              0
#define OTA_SN_ERROR        1
#define OTA_CHECKSUM_ERROR  2
#define OTA_FORMAT_ERROR    3
#define OTA_IMG_CRC_ERROR   4
#define OTA_IMG_OTA_OK      5 //end of OTA
#define OTA_UNKNOWN_ERROR 255
/*BLE OTA data proc API, return OTA_XXXX.
 OTA procedure is finised when the callback function OtaSystemReboot(void) is envoked.
 OTA_IMG_OTA_OK also means OTA has finished.
 void OtaSystemReboot(void) is porting function.
*/
u8 OTA_Proc(u8 *data, u8 len);//OTA lib supported ONLY!!!

//online sector erase API
void IAP_EraseSector(u32 adr/*MUST be 256 bytes aligned*/);

//online program API
void IAP_Program(u8* data/*MUST be SRAM addr*/,u8 len/*MUST be 4 bytes aligned*/, u32 pos/*MUST be 4 bytes aligned*/);

#endif
