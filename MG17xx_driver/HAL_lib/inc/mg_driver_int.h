/**
  ******************************************************************************
  * @file    mg_driver_int.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#ifndef __MG_DRIVER_INT_H__
#define __MG_DRIVER_INT_H__

#include "mg_driver_header.h"


#define INT_MASK_BIT_BB_SEG      0x00000001
#define INT_MASK_BIT_UART        0x00000002
#define INT_MASK_BIT_BB          0x00000004
#define INT_MASK_BIT_RTC         0x00000008
#define INT_MASK_BIT_WATCHDOG    0x00000010
#define INT_MASK_BIT_LOW_VOLTAGE 0x00000020
#define INT_MASK_BIT_GPIO        0x00000040
#define INT_MASK_BIT_TIMER       0x00000400
#define INT_MASK_BIT_IIC         0x00001000
#define INT_MASK_BIT_PWM_LED    INT_MASK_BIT_BB_SEG  /*share with seg irq*/
#define INT_MASK_BIT_SPI        0x00010000 //not used

typedef enum IRQn
{
    /******  Cortex-M0 Processor Exceptions Numbers ***************************************************/
    NonMaskableInt_IRQn         = -14,
    HardFault_IRQn              = -13,
    SVC_IRQn                    = -5,
    PendSV_IRQn                 = -2,
    SysTick_IRQn                = -1,
    
    BBSeg_IRQn                  = 0,
    UART_IRQn                   = 1,
    BB_IRQn                     = 2,
    RTC_IRQn                    = 3,
    WDG_IRQn                    = 4,
    LOW_VOLTAGE_IRQn            = 5,
    GPIO_IRQn                   = 6,
    
    TIMER_IRQn                  = 10,
    
    I2C_IRQn                    = 12,
} IRQn_Type;
/*
Interrupt enable :            0xe000_e100
Interrupt disable:            0xe000_e180
*/
#define REG_ENABLE_NVIC_IRQ   0xE000E100
#define REG_DISABLE_NVIC_IRQ  0xE000E180

void NVIC_DisableIRQ(u32 Irqn); //Irqn can be one of {INT_MASK_BIT_XXX}
void NVIC_EnableIRQ(u32 Irqn);  //Irqn can be one of {INT_MASK_BIT_XXX}
void NVIC_IrqSetPriority(u8 IrqIdx/*from 0 to max 31*/, u8 priority/*1,2,3*/);
void NVIC_IrqClearPendingIRQ(u8 IrqIdx/*from 0 to max 31*/);

void DisableEnvINT(void); //disable all interrupts started from systick interrupt(including)
void EnableEnvINT(void);  //enable all interrupts.

#endif

