/**
  ******************************************************************************
  * @file    mg_driver_lvd.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */

#ifndef __H_LOW_VOLTAGE_DECTECT_H__
#define __H_LOW_VOLTAGE_DECTECT_H__


//EnableFlag : 1-enabled monitor;  0-disabled monitor
void mg_EnableVddMonitor(unsigned char EnableFlag);

/*
warning voltage emLevelBits:
            0x00  1.83 V
            0x01  1.86 V
            0x02  1.89 V
            0x03  1.93 V
            0x04  1.97 V
            0x05  2.01 V
            0x06  2.06 V
            0x07  2.12 V  |--> ^2.16 V recover
fail voltage level is about 150mV lower than the warning voltage level
*/
void mg_SetVddMonitorLevel(unsigned char emLevelBits);

//if marning level met, return 1, otherwise return 0.
unsigned char mg_getVddWarningFlag(void);

//if fail level met, return 1, otherwise return 0.
unsigned char mg_getVddFailFlag(void);

#endif
