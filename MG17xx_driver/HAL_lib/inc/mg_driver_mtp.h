/**
  ******************************************************************************
  * @file    mg_driver_mtp.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */

#ifndef _MG_DRIVER_MTP_H_
#define _MG_DRIVER_MTP_H_


#define  MTP_ADDR_BASE  0x60040000

#define __IO  volatile
#define u32   unsigned long    

typedef struct _tag_MTP_REG{
    __IO u32 CMD_REG;
    __IO u32 STS;
    __IO u32 WDATA_REG;    
    __IO u32 RDATA_REG;
    __IO u32 TIMING_ADD1;
    __IO u32 TPGHF;
    __IO u32 TCERH;
}MTP_REG;

#define MTP_CTRL          ((MTP_REG *) MTP_ADDR_BASE)
#define MTP_CTRL_READ         (1 << 28)
#define MTP_CTRL_CHIP_ERASE   (2 << 28)
#define MTP_CTRL_SECTOR_ERASE (3 << 28)
#define MTP_CTRL_PROGRAM      (4 << 28)
#define MTP_CTRL_CAL          (5 << 28)

#define MTP_OPT_DONE          (0x03)

#define FLASH_SECTOR_SIZE (256)
//#define FLASH_PAGE_SIZE   (256)


void EraseChip(void);
int EraseSector(unsigned long adr);
int ProgramPage (unsigned long adr, unsigned long sz, unsigned char *buf);

#endif
