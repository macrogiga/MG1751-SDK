/**
  ******************************************************************************
  * @file    mg_driver_power.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
  
#ifndef _MG_DRIVER_POWER_H_
#define _MG_DRIVER_POWER_H_

#include "mg_driver_header.h"

typedef struct _tag_MG_POWER_CTRL
{
    __IO  u32 Reg;
}_MG_POWER_CTRL_;

#define MG_POWER_CTRL              ((_MG_POWER_CTRL_ *) (SYS_CTRL_BASE+0x14))

#define  POWER_BIT_AES_EN        0x00000010

void POWER_SetEnableBits(u32 PowerBits);
void POWER_SetDisableBits(u32 PowerBits);

#endif
