/**
  ******************************************************************************
  * @file    mg_driver_pwm.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
  
#ifndef _MG_DRIVER_PWM_H_
#define _MG_DRIVER_PWM_H_

#include "mg_driver_header.h"

typedef struct _tag_PWM_TypeDefAtom
{
  __IO  u32 PWM_DIV;  //16 bit
  __IO  u32 PWM_DUTY; //16 bit
}PWM_TypeDefAtom;

typedef struct _tag_PWM_TypeDef
{
  __IO  PWM_TypeDefAtom cfg[5];
  __IO  u32 PWM_ENABLE;     //5 bit
  __IO  u32 PWM_CLK_DIV_1;  //pwm source clock div coefficient,default is 255(256-1).  12bit
}PWM_TypeDef;

#define PWM              ((PWM_TypeDef *) PWM_BASE)

#define PWM_EN_CH0            0x01
#define PWM_EN_CH1            0x02
#define PWM_EN_CH2            0x04
#define PWM_EN_CH3            0x08
#define PWM_EN_CH4            0x10
#define PWM_DISABLE           0x00

#define PWM_CH0           0x00
#define PWM_CH1           0x01
#define PWM_CH2           0x02
#define PWM_CH3           0x03
#define PWM_CH4           0x04

void PWM_Init(u16 PwmIdx, u32 DutyInit/*16bit*/, u32 DurCount/*16 bit*/);//[PwmIdx] can be one of {PWM_CHx}
void PWM_SetValue(u16 PwmIdx,u32 DutyValue/*16bit*/);//[PwmIdx] can be one of {PWM_CHx}
void PWM_SetEnable(u8 EnableBitMap); //[EnableBitMap] can be one of {PWM_EN_CHx,PWM_DISABLE}
void PWM_SetClockDiv(u16 Div);

#endif
