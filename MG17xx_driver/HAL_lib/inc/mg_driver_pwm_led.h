/**
  ******************************************************************************
  * @file    mg_driver_pwm_led.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
  
#ifndef _MG_DRIVER_PWM_LED_H_
#define _MG_DRIVER_PWM_LED_H_

#include "mg_driver_header.h"


typedef struct _tag_PWM_LED_TypeDef
{    
    __IO  u32 Buf0;
    __IO  u32 Buf1;
    __IO  u32 Buf0Len;
    __IO  u32 Buf1Len;
    __IO  u32 LPWM_EN;
    __IO  u32 INT_EN;
    __IO  u32 FEED_INT_CLEAR;
    __IO  u32 END_INT_CLEAR;
    __IO  u32 INT_STATUS;
    __IO  u32 DutyCycle;  //LED pwm source clock, duty cycle number
    __IO  u32 DutyCount0; //for bit 0 duty count number(hight part)
    __IO  u32 DutyCount1; //for bit 1 duty count number(hight part)
    __IO  u32 RstCount;   //for rst period count number, keep low
}PWM_LED_TypeDef;

#define PWM_LED              ((PWM_LED_TypeDef *) PWM_LED_BASE)

#define PWM_LED_END_DATA_INT     0x00
#define PWM_LED_FEED_DATA_INT    0x01

//Buf0Addr and Buf1Addr SHOULD be 32bit aligned address.
//DutyCycleCount -- period
//Count0         -- 0-H
//Count0         -- 1-H
//RstCount       -- reset
void PWM_LED_Init(u32 Buf0Addr, u32 Buf1Addr, u32 DutyCycleCount, u32 Count0, u32 Count1, u32 RstCount);
void PWM_LED_Unint(void);

//connected to PWM4, one SHOULD select pin mux function to pwm4 first.
void PWM_LED_Start(void);

/*EndFlag == 1 means end part of data*/
void PWM_LED_UpdateDataLen0(u32 Len, u32 EndFlag);
void PWM_LED_UpdateDataLen1(u32 Len, u32 EndFlag);

//returns:
//   0: PWM_LED_END_DATA_INT
//   1: PWM_LED_FEED_DATA_INT
u8 PWM_LED_GetIntStatusValue(void);
void PWM_LED_ClearFeedInt(void);
void PWM_LED_ClearEndInt(void);

#endif
