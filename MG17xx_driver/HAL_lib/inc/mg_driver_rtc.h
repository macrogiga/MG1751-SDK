/**
  ******************************************************************************
  * @file    mg_driver_rtc.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */

#ifndef _MG_DRIVER_RTC_H_
#define _MG_DRIVER_RTC_H_

#include "mg_driver_header.h"


typedef struct _tag_MG_RTC
{
    __IO  u32 RTC_COUNT_SET; //24 bit
    __IO  u32 RTC_ENABLE;
    __IO  u32 RTC_INT_EN;
    __IO  u32 RTC_INT_STATUS; //read
    __IO  u32 RTC_INT_CLR;    //write
    __IO  u32 RTC_UNIT_SET;
    __IO  u32 RTC_CUR_COUNT; //read 24 bit
}_RTC_;

#define RTC              ((_RTC_ *) RTC_BASE)

void Rtc_Init(unsigned int Count /*unit 0.1s*/);
void Rtc_IntClear(void);
void Rtc_StartRtc(void);
void Rtc_StopRtc(void);

#endif
