/**
  ******************************************************************************
  * @file    mg_driver_spi_master.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
  
#ifndef _MG_DRIVER_SPI_MASTER_H_
#define _MG_DRIVER_SPI_MASTER_H_

#include "mg_driver_header.h"

typedef struct _tag_SPIM_TypeDef
{
  __IO  u32 CTRLR0;   //0
  __IO  u32 CTRLR1;   //4
  __IO  u32 SSIENR;   //8
  __IO  u32 MWCR;     //C
  __IO  u32 SER;      //10
  __IO  u32 BAUDR;    //14
  __IO  u32 TXFTLR;   //18
  __IO  u32 RXFTLR;   //1C
  __IO  u32 TXFLR;    //20
  __IO  u32 RXFLR;    //24
  __IO  u32 SR;       //28
  __IO  u32 IMR;      //2C
  __IO  u32 ISR;      //30
  __IO  u32 RISR;     //34
  __IO  u32 TXOICR;   //38
  __IO  u32 RXOICR;   //3c
  __IO  u32 RXUICR;   //40
  __IO  u32 MSTICR;   //44
  __IO  u32 ICR;      //48
  __IO  u32 ICR2;     //4c
  __IO  u32 DMATDLR;  //50
  __IO  u32 DMARDLR;  //54
  __IO  u32 IDR;      //58
  __IO  u32 SSI_COMP_VERSION; //5c
//  __IO  u32 DR;       //60
}SPIM_TypeDef;

#define SPIM_TX_ONLY  0x0100
#define SPIM_RX_ONLY  0x0200
#define SPIM_TX_RX    0x0000

#define SPIM_DFS      0x0007  /*8 bit*/
#define SPIM_FRF      0x0000  /*Motorola SPI*/

#define SPIM_SCPH_0   0x0000
#define SPIM_SCPH_1   0x0040

#define SPIM_SCPOL_0   0x0000
#define SPIM_SCPOL_1   0x0080


typedef struct _tag_SPIM_TypeInitDef
{
    u16 RxTxMode;
    u16 SCPOL;  //Motorola SPI
    u16 SCPH;    
    u16 SCK_DIV; //any even value between 2 and 65534
}SPIM_TypeInitDef;

#define SPIM_DATA_BASE           (__IO u32*)(SPIM_BASE+0x60)
#define SPIM               ((SPIM_TypeDef *) SPIM_BASE)

void SPIM_Init(SPIM_TypeInitDef* cfg);
void SPIM_TxRxBuffer(u8* dataIn, u8*dataOut, u16 len);

void SPIM_RxBufferReg(u8 Cmd, u8* DataOut, u16 Len); /*read Data(Len)*/
void SPIM_TxBufferReg(u8 Cmd, u8* DataIn, u16 Len); /*send Cmd+data*/
u8 SPIM_TxRxReg(u8 CmdReg, u8 Value); /* send CmdReg+Value*/

#endif
