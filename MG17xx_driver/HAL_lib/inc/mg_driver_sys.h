/**
  ******************************************************************************
  * @file    mg_driver_sys.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
  
#ifndef __MG_DRIVER_SYS_H__
#define __MG_DRIVER_SYS_H__

#include "mg_driver_header.h"


typedef struct _tag_MG_ROM_CTRL
{
    __IO  u32 Reg;
}_MG_ROM_CTRL_;

#define MG_SYS_ROM_CTRL              ((_MG_ROM_CTRL_ *) (SYS_CTRL_BASE+0x18))


typedef struct _tag_MG_STDALONE
{
    __IO  u32 Reg[2];
}_MG_STDALONE_;

#define SYS_STDALONE_REG              ((_MG_STDALONE_ *) (SYS_CTRL_BASE+0x20))

typedef struct _tag_MG_PMU_ENABLE
{
    __IO  u32 Pmu_enable;
}_MG_PMU_ENABLE_;

#define SYS_PMU_REG                  ((_MG_PMU_ENABLE_ *) (SYS_CTRL_BASE+0x2C))

typedef struct _tag_SYS_ROM_WAKEUP_DELAY_REG
{
    __IO  u32 Reg;
}_SYS_ROM_WAKEUP_DELAY_REG_;

#define SYS_ROM_WAKEUP_DELAY_REG     ((_SYS_ROM_WAKEUP_DELAY_REG_ *) (SYS_CTRL_BASE+0x34))

typedef struct _tag_SYS_PIN_SELECT_REG
{
    __IO  u32 Reg;
}_SYS_PIN_SELECT_REG_;

#define SYS_PIN_SELECT_REG     ((_SYS_PIN_SELECT_REG_ *) (SYS_CTRL_BASE+0x38))

typedef struct _tag_SYS_SPI_CS_REG_
{
    __IO  u32 Reg;
}_SYS_SPI_CS_REG_;
#define SYS_SPI_CS     ((_SYS_SPI_CS_REG_ *) (SYS_CTRL_BASE+0x3C))

typedef struct _tag_MG_ROM
{
    __IO  u32 CCR;
    __IO  u32 SR;
}_MG_ROM_CACHE_;

#define SYS_MG_ROM_CFG              ((_MG_ROM_CACHE_ *) (SYS_CTRL_ROM_BASE))

#define MCO_MASKBIT          (0x06)
#define MCO_EN_MCU_CLK_DIV4     (0x02)
#define MCO_EN_BB_CLK_16M_DIV4  (0x04)
#define MCO_EN_SYS_RC_CLK        MCO_MASKBIT
#define MCO_DISABLE          (0x00)

#define CACHE_PWR_OFF_EN     (0x08)

void SysTick_Init(u32 ticks /*24 bit*/);

void SysGotoSleepMode(void);
void SysGotoStopMode(void);   //BB wakeup source is auto-enabled, GPIO outside wakeup source may be added through GPIO_EXTInt_Init(GPIO,...);
void SysGotoStandbyMode(void);//the ONLY two PA0 and RTC wakeup sources are auto-enabled if any.


void Sys_StandaloneRegWrite(u8 Idx, u32 Value); //Idx can be 0,1
u32  Sys_StandaloneRegRead(u8 Idx);

void Sys_SpiSelect(u8 InternalEnableFlag/*default 0*/);
void Sys_SpiCsEnable(void);
void Sys_SpiCsDisable(void);

void Sys_ClockOutEnable(u8 McoEnClkID/*MCO_EN_XXXX,default MCO_DISABLE*/);
void Sys_EnableCachePowerOff(u8 EnableFlag/*default 0*/);

#endif
