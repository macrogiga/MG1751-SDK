/**
  ******************************************************************************
  * @file    mg_driver_timer.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
  
#ifndef _MG_DRIVER_TIMER_H_
#define _MG_DRIVER_TIMER_H_

#include "mg_driver_header.h"


//running mode
#define TIMER_FREE_RUNNING_MODE     0x00
#define TIMER_PERIODIC_RUNNING_MODE 0x40

void Timer1_Init(u32 load_value,u8 running_mode);
void Timer1_int_clear(void);

void Timer2_Init(u32 load_value,u8 running_mode);
void Timer2_int_clear(void);

u8 Timer1_int_status(void); //return 1 for int found
u8 Timer2_int_status(void); //return 1 for int found

#endif

