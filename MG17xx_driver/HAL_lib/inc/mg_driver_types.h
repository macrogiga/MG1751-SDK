/**
  ******************************************************************************
  * @file    mg_driver_types.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
  
#ifndef _MG_DRIVER_TYPE_H_
#define _MG_DRIVER_TYPE_H_

typedef unsigned char  u8;
typedef unsigned short u16;
typedef unsigned int   u32;

typedef char  su8;
typedef short su16;
typedef int   su32;

typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;
typedef enum {RESET = 0, SET = !RESET} ITStatus;

#define     __I     volatile const
#define     __O     volatile
#define     __IO    volatile   

#endif

