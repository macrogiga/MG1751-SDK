/**
  ******************************************************************************
  * @file    mg_driver_uart.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2020 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
  
#ifndef _MG_DRIVER_UART_H_
#define _MG_DRIVER_UART_H_

#include "mg_driver_header.h"

void UART_Set_baudrate(u32 bps);

u8   UART_Get_RX_Flag(void); //returns 1 means RX data ready
u8   UART_ReceiveByte(void); //read one byte
void UART_SendOneByte(u8 data);//send one byte
void UART_Enable_FlowControl(void); //Low level is active

void UART_ITConfig_TX(u8 enableFlag);
u32  UART_ReadClearInt(void);
u8   UART_Get_TX_Flag(void);

#endif
