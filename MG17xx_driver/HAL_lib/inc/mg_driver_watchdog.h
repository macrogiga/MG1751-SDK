/**
  ******************************************************************************
  * @file    mg_driver_watchdog.h
  * @author  
  * @version V1.0
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
  
#ifndef _MG_DRIVER_MATCHDOG_H_
#define _MG_DRIVER_MATCHDOG_H_

#include "mg_driver_header.h"

void WDog_Init(u32 load_value);
void WDog_Feed(u32 load_value);

#endif
