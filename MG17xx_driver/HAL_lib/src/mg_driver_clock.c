/**
  ******************************************************************************
  * @file    mg_driver_clock.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver_clock.h"


void CLK_SetDiv(u32 Div)
{
    if(Div == 0) //32k
    {
        SYS_CLK->CLK_CFG_CTRL = CLK_SEL_ENABLE | CLK_SEL_LOW;
    }
    else if(Div == 1)
    {
        SYS_CLK->CLK_CFG_CTRL = CLK_SEL_ENABLE;
    }
    else
    {
        Div --;
        if(Div > 15)Div = 15;
        
        SYS_CLK->CLK_CFG_CTRL = CLK_SEL_ENABLE | CLK_SEL_SYS_DIV_ENABLE | (Div << 3);
    }
}

u32 CLK_GetClockGateBits(void)
{
    return SYS_CLK->CLK_GATE;
}

void CLK_SetClockGateBits(u32 clk_gate)
{
    SYS_CLK->CLK_GATE = clk_gate;
}

//enable one module's clock
void CLK_SetClockGate(u32 clk_gate_bit)
{
    u32 t;
    
    t = SYS_CLK->CLK_GATE;
    SYS_CLK->CLK_GATE = t | clk_gate_bit;
}

//disable one module's clock
void CLK_ClearClockGate(u32 clk_gate_bit)
{
    u32 t;
    
    t = SYS_CLK->CLK_GATE & (~clk_gate_bit);
    SYS_CLK->CLK_GATE = t;
}
