/**
  ******************************************************************************
  * @file    mg_driver_int.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver_int.h"


void NVIC_DisableIRQ(u32 Irqn)
{
    __IO u32 *addr = (__IO u32 *)REG_DISABLE_NVIC_IRQ;
    
    *addr = Irqn;
}

void NVIC_EnableIRQ(u32 Irqn)
{
    __IO u32 *addr = (__IO u32 *)REG_ENABLE_NVIC_IRQ;
    
    *addr = Irqn;
}


static char dis_int_count = 0;
#define __ASM __asm
void DisableEnvINT(void)
{
    //to disable int
    __ASM volatile("cpsid i");
    
    dis_int_count ++;
}

void EnableEnvINT(void)
{
    //to enable/recover int
    dis_int_count --;
    if(dis_int_count<=0) //protection purpose
    {
        dis_int_count = 0; //reset
        __ASM volatile("cpsie i");
    }
}
