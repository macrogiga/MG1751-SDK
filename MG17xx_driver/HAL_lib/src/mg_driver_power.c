/**
  ******************************************************************************
  * @file    mg_driver_power.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */

#include "mg_driver_power.h"

void POWER_SetEnableBits(u32 bits)
{
    u32 t;
    
    t = MG_POWER_CTRL->Reg;
    MG_POWER_CTRL->Reg = t | bits;
}

void POWER_SetDisableBits(u32 bits)
{
    u32 t;
    
    t = MG_POWER_CTRL->Reg;
    MG_POWER_CTRL->Reg = t & (~bits);
}
