/**
  ******************************************************************************
  * @file    mg_driver_pwm_led.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver_pwm_led.h"
#include "mg_driver_clock.h"
#include "mg_driver_gpio.h"



//connected to PWM4, one SHOULD select pin mux function to pwm first.
void PWM_LED_Start(void)
{
    PWM_LED->LPWM_EN = 0x03; 
}

void PWM_LED_Unint(void)
{
    PWM_LED->LPWM_EN = 0;
}

//Buf0 and Buf1 are 32bit aligned
void PWM_LED_Init(u32 Buf0Addr, u32 Buf1Addr, u32 DutyCycleCount, u32 Count0, u32 Count1, u32 RstCount)
{
    //enable clk
    CLK_SetClockGate(CLK_GATE_LED_PWM_EN);
    
    //PWM pin mux to PWM4 (fixed IO)
    GPIO_FunctionSelect(GPIO_MAP_PIN_A10,GPIO_MAP_PIN_FUNC2);
    SysCtrl->PA_CONF[10] = 0x04; // GPIO Push-Pull mode
    
    PWM_LED->Buf0 = Buf0Addr;
    PWM_LED->Buf1 = Buf1Addr;
    
    PWM_LED->INT_EN = 0x03;
    PWM_LED->END_INT_CLEAR = 1;
    PWM_LED->FEED_INT_CLEAR = 1;
    
    PWM_LED->DutyCount0 = Count0;
    PWM_LED->DutyCount1 = Count1;
    PWM_LED->DutyCycle = DutyCycleCount;
    PWM_LED->RstCount = RstCount;
}

void PWM_LED_UpdateDataLen0(u32 Len, u32 EndFlag/*1 means end of data flag*/)
{
    if(EndFlag) Len |= (0x01 << 10);
    PWM_LED->Buf0Len = Len;
}

void PWM_LED_UpdateDataLen1(u32 Len, u32 EndFlag/*1 means end of data flag*/)
{
    if(EndFlag) Len |= (0x01 << 10);
    PWM_LED->Buf1Len = Len;
}

void PWM_LED_ClearFeedInt(void)
{
    PWM_LED->FEED_INT_CLEAR = 1;
}

void PWM_LED_ClearEndInt(void)
{
    PWM_LED->END_INT_CLEAR = 1;
}

//PWM_LED_END_DATA_INT  : 0
//PWM_LED_FEED_DATA_INT : 1
u8 PWM_LED_GetIntStatusValue(void)
{
    return (PWM_LED->INT_STATUS & 0x01);
}
