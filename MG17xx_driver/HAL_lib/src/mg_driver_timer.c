/**
  ******************************************************************************
  * @file    mg_driver_timer.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver_timer.h"
#include "mg_driver_int.h"
#include "mg_driver_clock.h"


void Timer1_Init(u32 load_value, u8 running_mode)
{   
    CLK_SetClockGate(CLK_GATE_TIMER_EN);
    
    //enable the irq bit
    NVIC_EnableIRQ(INT_MASK_BIT_TIMER);
    
    TIMER_ADDR->TIMER1LOAD = load_value; 
    //32 bit, 
    TIMER_ADDR->TIMER1CONTROL = 0xa2 | running_mode;
}

void Timer1_int_clear(void)
{
    TIMER_ADDR->TIMER1INTCLR = 1;
}

void Timer2_Init(u32 load_value, u8 running_mode)
{    
    CLK_SetClockGate(CLK_GATE_TIMER_EN);
    
    //enable the irq bit
    NVIC_EnableIRQ(INT_MASK_BIT_TIMER);
    
    TIMER_ADDR->TIMER2LOAD = load_value;
 
    //32 bit, 
    TIMER_ADDR->TIMER2CONTROL = 0xa2 | running_mode;
}

void Timer2_int_clear(void)
{
    TIMER_ADDR->TIMER2INTCLR = 1;
}

u8 Timer1_int_status(void)
{
    if(TIMER_ADDR->TIMER1RIS)return 1;
    
    return 0;
}

u8 Timer2_int_status(void)
{
    if(TIMER_ADDR->TIMER2RIS)return 1;
    
    return 0;
}
