/**
  ******************************************************************************
  * @file    mg_driver_watchdog.c
  * @author  
  * @version V1.1
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2019 Shanghai Macrogiga Electronics</center></h2>
  *
  ******************************************************************************
  */
#include "mg_driver_watchdog.h"
#include "mg_driver_int.h"
#include "mg_driver_clock.h"


#define WDG_INT_EN  0x01
#define WDG_RSET_EN 0x02

void WDog_Init(u32 load_value)
{
    CLK_SetClockGate(CLK_GATE_WATCHDOG_EN);
    
    WDOG->WDOGLOAD = load_value;
    WDOG->WDOGCONTROL = WDG_RSET_EN | WDG_INT_EN;
}

__asm void WDog_Feed (u32 load_value)
{
    PRESERVE8
    PUSH    {R4,LR}
    LDR     R3, [PC,#36]
    LSLS    R3, R3, #6
    LDR     R1, [R3,#12]
    ASRS    R4, R3, #19
    BICS    R1, R1, R4
    STR     R1, [R3,#12]
    
    MOVS    R1, #0
    MOVS    R2, #21
    LSLS    R2, R2, #5
LOOP
    ADDS    R1, R1, #1
    UXTH    R1, R1
    CMP     R1, R2
    BCC     LOOP
    
    LDR     R1, [PC,#12]
    STR     R0, [R1,#0]
    
    LDR     R0, [R3,#12]
    ORRS    R0, R0, R4
    STR     R0, [R3,#12]
    
    POP     {R4,PC}

    DCD     0x010007c0
    DCD     0x40008000

    ALIGN
}
