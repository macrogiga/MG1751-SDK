# MG1751-SDK

#### Description
SDK for MG1751, ARM Cortex-M0 core, BLE5.0 SoC(QFN24)

#### Software Architecture
Software architecture description
MG1751-SDK
├─Apps
├─Docs
├─Examples
│  ├─Demo_Bt
│  │  ├─obj
│  │  └─Src
│  ├─Demo_Bt_iBeacon
│  │  ├─obj
│  │  └─Src
│  └─Demo_Bt_Profiles
│      ├─obj
│      └─Src
├─MG17xx_driver
│  ├─BT_lib
│  ├─CMSIS
│  │  └─CM0
│  └─HAL_lib
│      ├─boot
│      ├─inc
│      └─src
├─Utils
└─MG1751.FLM

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. Use Keil_v5 to develop or debug
2. Debug tools include Jlink, Ulink2 or CMSIS-DAP Debugger
3. For flash programming, please copy MG1751.FLM to [Keil install folder]\ARM\Flash\

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)